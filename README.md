**Project is being developed on a private fork. The full spectrum of code and data will be available after publication.**

# MTBC Immunoinformatics 

- [Dataset definition](#dataset-definition)
  - [Get list of candidate isolates](#get-list-of-candidate-isolates)
  - [Build representative list of genomes](#build-representative-list-of-genomes)
  - [Download genomes](#download-genomes)
- [NGS helper files](#ngs-helper-files)
  - [Reference genome](#reference-genome)
  - [Genome annotation](#genome-annotation)
  - [Problematic genomic regions](#problematic-genomic-regions)
  - [Lineage and sublineage markers](#lineage-and-sublineage-markers)
  - [T cell epitopes](#t-cell-epitopes)
  - [Sequencing adapters](#sequencing-adapters)
- [NGS analysis](#ngs-analysis)
  - [Pipeline](#pipeline)
  - [Lineage typing](#lineage-typing)

Dataset definition
=====================

Get list of candidate isolates
-------

The goal was to find a globally representative dataset, ideally with a sufficient number of genomes from each MTBC sublineage. The reference for typing was the latest TBProfiler paper [Napier et al Genome Medicine Dec 2020 paper](https://doi.org/10.1186/s13073-020-00817-3), which used 35,298 genomes. Other comprehensive studies were taken into account, namely:


- [Stucki et al Nature Genetics Oct 2016 (L4 specialists and generalists paper)](https://doi.org/10.1038/ng.3704), which included 1099 genomes (sublineage typing available).
- [Brynildsrud et al Science Advances Oct 2018 (global expansion of L4 study)](https://doi.org/10.1126/sciadv.aat5869), which screened 1669 genomes (sublineage typing available, but only 2-digit resolution).
- [O'Neill et al, Molecular Ecology May 2019 (Lineage specific histories)](https://doi.org/10.1111/mec.15120), which used 567 L1-L7 genomes (lineage typing available).
- [Chiner-Oms et al Science Advances Jun 2019](https://doi.org/10.1126/sciadv.aaw3307). which used a global dataset of 5560 strains (sublineage typing available).
- [Menardo et al PLoS Pathogens Sep 2019](https://doi.org/10.1371/journal.ppat.1008067), which screened a total of 21,734 genomes to select "6,285 strains with high quality genome sequences and an associated date of sampling".
- [Furió et al Commun Biol Nov 2021](https://doi.org/10.1038/s42003-021-02846-z), which contains a list of 4762 globally representative genomes frequently used by Iñaki Comas-led group.
- [Freschi et al bioRxiv Sep 2020 *fast-lineage-caller* paper](https://doi.org/10.1101/2020.09.29.293274), which screened 9,584 "high-quality" genomes and a larger dataset of 17,432 isolates sampled from 74 countries (sublineage typing available in the former set). This study uses a different typing scheme, where each subdivision in the classification corresponds to a split in the phylogenetic tree of each major MTBC lineage (L1 to L4). Correspondence to Coll et al scheme seems to be provided when needed.
- [Coscolla et al Microbial Genomics Feb 2021](https://doi.org/10.1099/mgen.0.000477). This paper used 675 isolates to highlight 7 L5 sublineages and 9 L6 sublineages (sublineage typing available).
- [Menardo et al F1000Research Mar 2021](https://doi.org/10.12688/f1000research.28318.2), which analyzed 2,938 L1 and 2,030 L3 whole genome sequences originating from 69 countries (sublineage typing available). Includes accession numbers for 17,218 L2 and L4 isolates as well. This might be the most resource for a curated, high quality list of L1-L4 genomes (only lineage typing is provided).

More focused/geographically restricted studies were also taken into account, namely:

- [Holt et al, Nature Genetics May 2018 (EsxW Beijing Vietnam study)](https://doi.org/10.1038/s41588-018-0117-9), which sequenced 1635 genomes and includes information for another 3144 genomes (such as the ones from Comas 2013). Sublineage typing is available for the two datasets.
- [Palittapongarnpim et al, Nature Scientific Reports Aug 2018 (Thai L1 isolates study)](https://doi.org/10.1038/s41598-018-29986-3). A specific sublineage typing is available.
- [Liu et al PNAS Apr 2021](https://doi.org/10.1073/pnas.2017831118), which sequenced 576 genomes from Tibetan Plateau.

Code for this step is contained in [**Scripts/1-Dataset_definition-getlists.sh**](Scripts/1-Dataset_definition-getlists.sh).

Build representative list of genomes
-------

The goal was to build a list of genomes representing all the known sublineages and internal groups, namely the ones with described geographic restriction (mainly on stucki2016, freshi2020 and menardo2021 papers).
Code for this step is contained in [**Scripts/2-Dataset_definition-buildrepresentativelist.sh**](Scripts/2-Dataset_definition-buildrepresentativelist.sh).

Download genomes
-------
The final list of genomes was downloaded from public repositories with [**Scripts/3-Dataset_definition-downloadgenomes.sh**](Scripts/3-Dataset_definition-downloadgenomes.sh).

NGS helper files
=====================
This section retrieved or generated files needed to perform NGS analysis and annotation. Those comprised reference genome, sequencing adapters, problematic genomic regions, MTBC lineage typing, genome annotation and T cell epitopes.

Reference genome
-------
NGS reads were mapped to the inferred most recent common ancestor (MRCA) of the MTBC, obtained in [Comas et al Nature Genetics 2010](https://doi.org/10.1038/ng.590).
The DNA sequence of this inferred MTBC ancestral has the same length as H37Rv strain, the first *M. tuberculosis* strain ever sequenced and consequently used as a reference in many studies. However, H37Rv belongs to lineage 4.10 and mapping to it leads to a high number of variants in phylogenetically distant strains. Therefore, the normalisation to the MTBC ancestor is important, and a growing number of studies have been incorporating this notion.

Code for this step is contained in [**Scripts/4-NGS_helper_files-referencegenome.sh**](Scripts/4-NGS_helper_files-referencegenome.sh).

Genome annotation
-------
The annotation file containing information about the start/end position of coding regions (CDSs) was based on [H37Rv reference genome](https://www.ncbi.nlm.nih.gov/nuccore/NC_000962.3).

Code for this step is contained in [**Scripts/5-NGS_helper_files-genomeannotation.sh**](Scripts/5-NGS_helper_files-genomeannotation.sh).

Problematic genomic regions
-------
[Marin et al Bioinformatics Jan 2022](https://doi.org/10.1093/bioinformatics/btac023) compared Illumina and PacBio sequencing on 36 phylogenetically diverse isolates (L1-L8, L7 isolates excluded) to find 773 **refined low confidence (RLC) regions** of H37Rv reference genome. Those **RLC** included 30 false positive SNP hot spots; genomic regions with low recall, as per the **Empirical Base-level Recall** metric (**EBR**, calculated as the proportion of isolates for which Illumina variant calling made a confident variant call that agreed with the PacBio ground truth) < 0.9; and regions ambiguously defined by long-read sequencing (i.e. with variable copy number relative to the H37Rv reference genome or difficult to align due to a high level of sequence divergence). In addition, **pileup mappability** was used to assess the uniqueness of genomic regions. That was defined as the average mappability of all overlapping k-mers (metric calculated for each genomic position). Scores range from 0-1 and might be used to assess confidence of variant calls.

Code for this step is contained in [**Scripts/6-NGS_helper_files-problematicgenomicregions.sh**](Scripts/6-NGS_helper_files-problematicgenomicregions.sh).

Lineage and sublineage markers
-------
Genomic markers to assign MTBC lineages and sublineages were retrieved from:

- [*Mykrobe* pipeline, Hunt et al (Zamin Iqbal group) Dec 2019, with first release in 2015](https://wellcomeopenresearch.org/articles/4-191), which uses "Chiner-Oms 2020 lineage scheme" from v0.9.0.
- [Freschi et al Nature Communications Oct 2021 *fast-lineage-caller* paper](https://doi.org/10.1038/s41467-021-26248-1), which contains a [list of 96 barcoding SNPs](https://github.com/farhat-lab/fast-lineage-caller/blob/master/snp_schemes/freschi.tsv) (sublineages only for lineages 1-4).
- [*TBProfiler* pipeline](https://github.com/jodyphelan/TBProfiler), [Phelan et al Genome Medicine Jun 2019](https://doi.org/10.1186/s13073-019-0650-x). Last versions are based on [Napier et al Genome Medicine Dec 2020 paper](https://doi.org/10.1186/s13073-020-00817-3), which used "35,298 MTBC isolates (~ 1 million SNPs)" to obtain a list of 90 "minimal barcoding SNPs" for all MTBC lineages/sublineages.
- [Coscolla et al Microbial Genomics Feb 2021](https://doi.org/10.1099/mgen.0.000477). This paper used 675 isolates to highlight 7 L5 sublineages and 9 L6 sublineages.

Code for this step is contained in [**Scripts/7-NGS_helper_files-lineageandsublineagemarkers.sh**](Scripts/7-NGS_helper_files-lineageandsublineagemarkers.sh).

T cell epitopes
-------
T cell epitopes from the MTBC were obtained from:

- [Menardo et al F1000Research Mar 2021](https://doi.org/10.12688/f1000research.28318.2), which "analysed 2938 L1 and 2030 L3 whole genome sequences originating from 69 countries" and inspected amino acid variants in experimentally validated T cell epitopes.
- [Vargas et al eLife Feb 2021](https://doi.org/10.7554/eLife.61805), which downloaded known T cell epitopes from the [IEDB database](https://www.iedb.org/) and mapped their coordinates to H37Rv genome.

Code for this step is contained in [**Scripts/8-NGS_helper_files-tcellepitopes.sh**](Scripts/8-NGS_helper_files-tcellepitopes.sh).

Sequencing adapters
-------
A comprehensive list of sequencing adapters to be removed from each genome was retrieved with [**Scripts/9-NGS_helper_files-sequencingadapters.sh**](Scripts/9-NGS_helper_files-sequencingadapters.sh).

NGS analysis
=====================

Pipeline
-------

![Schematic description of the NGS pipeline](Figures/Readme_NGSpipeline.png)

Code for this step is contained in [**Scripts/10-NGS_analysis-pipeline.sh**](Scripts/10-NGS_analysis-pipeline.sh).

Lineage typing
-------
For all the genomes analysed in [*Pipeline*](#pipeline) (n = 13064), [*Lineage and sublineage markers*](#lineage-and-sublineage-markers) were extracted from the Variant Call Format (VCF) files. Typing info from unfiltered VCF files was compared after the removal of sites belonging to refined low confidence (RLC) regions or with:
- Empirical Base-level Recall (EBR) score < 0.9
- variant frequency < 75%
- pileup mappability < 1
- average mapping quality (MQ) < 40

We selected samples belonging to MTBC lineages 1 to 7 (L1 to L7) and with:
- a single assigned sublineage within each typing scheme
- the same lineage across typing schemes
- percentage of MTBC DNA >= 90%
- covered by >=10 mapped reads in >=95% of the reference genome length

A total of 11580 genomes proceeded to the next phase.

Code for this step is contained in [**Scripts/11-NGS_analysis-lineagetyping.sh**](Scripts/11-NGS_analysis-lineagetyping.sh).
