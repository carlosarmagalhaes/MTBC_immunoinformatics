cd ../Dataset_definition ;

echo "Retrieve typing schemes" ;
#---------------------------------------------------------------------------------------
wget "https://github.com/farhat-lab/fast-lineage-caller/raw/master/snp_schemes/freschi_hierarchical.tsv" -O typingscheme_freschihierarchical.tsv ;
## seems to be what was used in the paper (v0.3.2 of the tool introduced a simplified name scheme, see https://github.com/farhat-lab/fast-lineage-caller)
wget "https://github.com/farhat-lab/fast-lineage-caller/raw/master/snp_schemes/coll.tsv" -O typingscheme_coll2014.tsv ;
wget "https://github.com/farhat-lab/fast-lineage-caller/raw/master/snp_schemes/stucki.tsv" -O typingscheme_stucki2016.tsv ;
wget "https://github.com/jodyphelan/TBProfiler/raw/cfb370ac2ca6d9ec0b7e3b39d0a9c929ec648639/db/tbdb.barcode.bed" -O typingscheme_tbprofiler.bed ;


echo "Inspect typing schemes" ;
#---------------------------------------------------------------------------------------
cat typingscheme_coll2014.tsv typingscheme_stucki2016.tsv \
| less \
| sed -e '/^#/d' -e 's/lineage//g' -e 's/\*//g' \
| less \
| awk -F'\t' '{print $NF,$1}' OFS='\t' \
| less \
| sed -e 's/\/.*//g' \
| less \
| datamash -s -g 2 unique 1 \
| less \
>typingscheme_collandstucki.tsv ;
cat -n typingscheme_collandstucki.tsv ;
## 64 distinct classifications (2 for Bovis). This number seems to be same for biohansel (https://github.com/phac-nml/biohansel/blob/development/bio_hansel/data/tb_lineage/metadata.tsv)
### stucki2016 updated sublineage-SNPs for some sublineages (although Coll ones should be valid as well, but maybe the former are more robust)
cat -n typingscheme_freschihierarchical.tsv ;
## 95 distinct classifications, although the Supplementary File 2 lists a total of 104 (low FST is one of the reasons for the difference...)
### includes several 'internal groups', which are not true sublineages (as better explained in the paper)
less typingscheme_tbprofiler.bed \
| cut -f4 \
| sort -u \
| cat -n ;
## 110 distinct classifications, 106 if excluding L8,L9, M.caprae and M.orygis
### no 4.10/PGG3 group. Worthy checking if 4.10 strains are classified as 4.7, 4.8 or 4.9...
### other sublineages such as Uganda or Harleem seem to follow the same classification of Coll + Stucki...
## the problem with this typing scheme is that it does not provide correspondence to Coll et al, such as in Freschi2020!


echo "Get Run IDs for Freschi2020 high-quality dataset" ;
#---------------------------------------------------------------------------------------
## an alternative to this procedure might be a search in https://www.ncbi.nlm.nih.gov/sra or https://www.ncbi.nlm.nih.gov/biosample
mkdir -p getrunids ;
i=0 ; 
time less dataset_freschi2020_highquality.biosample \
| cut -f1 \
| while read biosample ;
  do echo "$biosample" ; 
    wget "http://trace.ncbi.nlm.nih.gov/Traces/sra/sra.cgi?save=efetch&db=sra&rettype=runinfo&term=$biosample" -O getrunids/"$biosample".csv ;
    ((++i)) ; echo i="$i" ;
done ;
## ≃ 1h26m
ls getrunids/*.csv \
| less \
| xargs cat \
| less \
| sed -e '/^$/d' -e '/^Run/d' -e 's/MICROBIOLOGIE,/MICROBIOLOGIE/g' -e 's/,/\t/g' \
| less \
>dataset_freschi2020_highquality_ncbinfo.tsv ;
## get simplifed run_id,biosample file:
less dataset_freschi2020_highquality_ncbinfo.tsv \
| awk -F'\t' '{print $1,$26,$16}' OFS='\t' \
| less \
| datamash -s -g 2 unique 1,3 countunique 1 \
| less \
| awk -F'\t' '{split($2,runids,",") ; for(i=1;i<=length(runids);i++) print runids[i],$0}' OFS='\t' \
| less \
>dataset_freschi2020_highquality.tsv ;
less dataset_freschi2020_highquality.tsv ;
## 7712 biosamples, but 10840 rows...
### some biosamples have multiple associated runs...
grep -i 'single' dataset_freschi2020_highquality.tsv ;
## 8 samples with a paired-end + single-end run associated. Maybe the paired-end run is a new, independent sequencing run of the same biosample...
rm -rf getrunids/ ;


echo "Assess representativity of datasets" ;
#---------------------------------------------------------------------------------------
ls ;

## Single and paired-end "mycobacterium tuberculosis" genomes got by searching on  https://www.ncbi.nlm.nih.gov/sra -> Send to -> Create File -> Run Info -> saved as 'paired-endgenomes_mtb_ncbi.csv' and 'single-endgenomes_mtb_ncbi.csv'!
### those genomes will be used to filter out other lists, as we prefer to have only paired-end genomes in the final...

# studies with Coll or Stucki typing info (n=6):
cat dataset_brynildsrud2018.tsv dataset_chineroms2019.tsv dataset_holt2018_globalset.tsv dataset_holt2018_study.tsv <(less dataset_menardo2021_L1toL4.tsv | awk -F'\t' '$NF=="menardo2021L1L3"' OFS='\t' | less) <(less dataset_pepperell2019.tsv | awk -F'\t' '$(NF-1)!=""' OFS='\t' | less) dataset_stucki2016.tsv \
| less \
| awk -F'\t' '{print $1,$NF,$(NF-1)}' OFS='\t' \
| less \
| sed -e 's/\/.*//g' -e '/^NA/d' -e 's/Lineage 4 sublineage definitions/4/g' -e 's/\tL/\t/g' \
| less \
| datamash -s -g 1 unique 3,2 countunique 3,2 \
| less \
>dataset_joinedcollorstucki.tsv ;
## 13386 unique run IDs (some will belong to the same biosample)!
less dataset_joinedcollorstucki.tsv \
| awk -F'\t' '{if($2 ~ "4.10,4.7$" || $2 ~ "4.10,4.8$" || $2 ~ "4.10,4.9$"){split($2,typing,",") ; $1=$1 FS typing[length(typing)] ; print $0} else if($2=="Possible_coinfection"){$1=$1 FS "mixture" ; print $0} else {split($2,typing,",") ; mixture=0 ; for(i=1;i<length(typing);i++){if(typing[i+1] !~ "^"typing[i]){mixture++}} if(mixture>0){$1=$1 FS "mixture"} else {$1=$1 FS typing[length(typing)]} print $0}}' OFS='\t' \
| less \
>dataset_joinedcollorstucki_treated.tsv ;
### the classification with highest resolution was chosen for the 2nd field!
grep 'mixture' dataset_joinedcollorstucki_treated.tsv \
| cat -n ;
## 4 strains with with inconsistent classifications among studies classifed as "mixture"
## 42 strains flagged in chineroms2019 as "possible co-infection" were also classified as "mixture"
### this is a good coll+stucki typed genome list to assess representativity:
less dataset_joinedcollorstucki_treated.tsv \
| grep -v 'mixture' \
| cut -f2 \
| sort \
| uniq -c \
| sort -gk2,2 \
| cat -n ;
## 48/62 classifications found.
### high numbers of L1 and L3 due to comprehensive sampling in Menardo2021
### high numbers of 2.2.1 due to holt2018 study
### low numbers in some L4 sublineages, need to increase
## we might get a sense if we can remove menardo2021 or holt2018 studies (to decrease excessive representation of L1, L2 and L3 lineages) by intersecting them with other sources with less isolates of those lineages...
less dataset_joinedcollorstucki_treated.tsv \
| cut -f2 \
| sort \
| uniq -c \
| awk '{print $2}' \
| awk 'NR==FNR {typingfound[$1] ; next} !($1 in typingfound)' /dev/stdin <(cut -f1 typingscheme_collandstucki.tsv | less) \
| cat -n ;
## 15 4-digit + 1 5-digit Coll groups were not found in the datasets -> might need to be found elsewhere... -> e.g. there are 353 2.2.1.1 strains in mclock2019!!
## 14/16 missing groups are from L4 and all can be found (tbprofiler typing though) in peerj large scale evaluation study, some of them in very high numbers (1 or 2 with low numbers on the other hand...). Some missing groups can be found in Freshi2020 selected set as well!
### guess what? all Coll groups are in the 2014 study, although some with low numbers...
## on the other hand, 1.2 group (n=22 run ids) was not found in Coll+Stucki and thus might need to be removed...

# compare the number of L5-L6 genomes in TBprofiler2020 to Coscolla 2021 study: 
less dataset_tbprofiler2020.tsv \
| awk -F'\t' '$3>=5 && $3<8' OFS='\t' \
| less \
| cut -f2 \
| sort -u \
| less ;
## 512 L5-L7 genomes in TBprofiler. So, the typing scheme of Coscolla2021 will be preferred due to higher number of genomes used!
### Curiously, Coscolla 2021 did not use any L7 strain, so the 64! L7 genomes in TBprofiler2020 will be considered for the final list!

# compare typing scheme of coll+stucki with TBprofiler2020:
less dataset_joinedcollorstucki_treated.tsv \
| awk -F'\t' 'NR==FNR {typing[$1]=$2 ; next} ($1 in typing) {print $1,typing[$1],$3}' OFS='\t' /dev/stdin <(less dataset_tbprofiler2020.tsv) \
| less \
| awk -F'\t' '$2!="mixture" && $2<5 && $2!=$3' OFS='\t' \
| less \
| awk -F'\t' '{if($3 !~ "^"$2 && length($2)>length($3)) print $0,"collhigherres" ; else if($3 !~ "^"$2 && length($2)==length($3)) print $0,"colldifftype" ; else print $0}' OFS='\t' \
| less \
| grep 'coll' \
| less \
| awk -F'\t' '$2!="4.10"' OFS='\t' \
| less ;
## from 11093 run ids found in tbprofiler2020 dataset, classification is different in 5350 cases.
### 38 cases with higher resolution in Coll and 323 with distinct classifications!
## all 4.10 Coll strains are classified as 4.7 to 4.9 in TBprofiler (except 5 that were classified as L4 only) !!
### Overall, agreement is very high, considering that the vast majority of the disagreements is due to additional level(s) of resolution in TBprofiler2020!
## isolates with classification disagreements between Coll+Stucki and TBprofiler2020 might be discarded later...

less dataset_freschi2020_highquality.biosample \
| cut -f1 \
| grep -v -w -F -f <(less dataset_freschi2020_largerset.biosample | cut -f1) /dev/stdin \
| less ;
## 5022/7712 biosamples of the high-quality set not present in the larger set!
less dataset_freschi2020_highquality.tsv \
| cut -f1 \
| grep -w -F -f /dev/stdin dataset_joinedcollorstucki_treated.tsv \
| less \
| cut -f2 \
| sort \
| uniq -c \
| cat -n ;
## 42 Coll classifications found in 5302 runids!
### some groups have low numbers, namely in L1 and L2...
### according to supplementary data, there are many internal groups with < 10 isolates, so we might need to enrich those groups by searching in the larger dataset!

less dataset_furio2020.tsv \
| cut -f1 \
| grep -v -w -F -f <(less dataset_molecularclock2019_filtered.tsv | cut -f1) /dev/stdin \
| less ;
## 4060/4750 isolates of the Iñaki global set are not in mclock2019 dataset! (strange)
less dataset_furio2020.tsv \
| cut -f1 \
| grep -v -w -F -f <(less dataset_chineroms2019.tsv | cut -f1) /dev/stdin \
| less ;
## only 105/4750 isolates of the Iñaki global set are not in chineroms2019 dataset!
grep -w -F -f <(cut -f1 dataset_furio2020.tsv) dataset_joinedcollorstucki_treated.tsv \
| less \
| cut -f2 \
| sort \
| uniq -c \
| cat -n ;
## 46/62 Coll+Stucki classifications found in Chineroms2019 dataset (n=4714 runids=almost everyting)... However, many groups have low numbers of isolates...
### it might be better to download just what's needed from Iñaki's group datasets...

# assess the representativity of menardo2019+menardo2021 datasets:
less dataset_molecularclock2019_filtered.tsv \
| cut -f3 \
| grep -v -w -F -f <(less dataset_menardo2021_L1toL4.tsv | cut -f3 | less) /dev/stdin \
| sort -u \
| less ;
## 2400 samples of menardo2019 are not found in menardo2021!
less dataset_molecularclock2019_filtered.tsv \
| cut -f1 \
| grep -w -F -f /dev/stdin dataset_joinedcollorstucki_treated.tsv \
| less \
| cut -f2 \
| sort \
| uniq -c \
| cat -n ;
## 34 Coll classifications found in 4503 runids!
### very large representation of 2.2.1 in comparison with others, so it does not make much sense...
### many groups with low number of isolates...

cat dataset_thai2018.tsv dataset_tibete2021.tsv \
| less \
| cut -f1 \
| grep -w -F -f /dev/stdin dataset_joinedcollorstucki_treated.tsv \
| less \
| cut -f2 \
| sort \
| uniq -c \
| cat -n ;
## some very nice numbers for 7 L1 groups!
### no hits found for the Tibet dataset, as expected...


echo "Get first candidate genome list for download" ; 
#---------------------------------------------------------------------------------------
cat dataset_brynildsrud2018.tsv <(grep -v 'Bovis' dataset_chineroms2019.tsv) dataset_freschi2020_highquality.tsv dataset_pepperell2019.tsv dataset_stucki2016.tsv dataset_thai2018.tsv \
| less \
| cut -f1 \
| less \
| cat /dev/stdin <(less dataset_coscolla2021.tsv | awk -F'\t' '$3!="L9" {print $1}') \
| less \
| sed -e 's/ERX3198376/ERR3170430/g' -e 's/ERX512033/ERR552964/g' -e '/SRX007722/d' -e '/SRR022876/d' -e '/SRX007725/d' -e '/SRR022879/d' -e '/SRX007726/d' -e '/SRR022880/d' \
| less \
| grep -v -w -F -f <(awk -F',' 'NR>1 {print $1}' single-endgenomes_mtb_ncbi.csv | sed -e '/^$/d' | less) /dev/stdin \
| less \
| grep -v -w -F -f <(less dataset_joinedcollorstucki_treated.tsv | awk -F'\t' '$2=="mixture"' OFS='\t' | less | cut -f1) /dev/stdin \
| less \
| grep -v -w -F -f <(less dataset_tbprofiler2020.tsv | awk -F'\t' '$3=="Bovis" || $3=="orgis" || $3=="caprae" || $3==8 || $3==9 {print $1}') \
| sort -u \
| less \
| awk '{print $1,"https://sra-pub-run-odp.s3.amazonaws.com/sra/"$1"/"$1}' OFS='\t' \
| less \
>download_firstroundlist.tsv ;
## 15699 run ids! (some will be part of the same biosample)
### L5, L6 (better provided by Coscolla2021 study) and "mixture" genomes excluded
### some ID correction and single-end removal in Coscolla2021...
grep 'X' download_firstroundlist.tsv ;
## no hits, so runids appear to be fine!
less paired-endgenomes_mtb_ncbi.csv \
| sed -e '1d' -e 's/,.*//g' -e '/^$/d' \
| cut -f1 \
| sort -u \
| less \
| grep -v -w -F -f /dev/stdin download_firstroundlist.tsv \
| less \
| cat -n ;
## 19 run ids not found in paired-end genome list!
### Nevertheless, manual search (e.g. https://www.ncbi.nlm.nih.gov/sra/?term=ERR651004) only found 'ERR553395' and 'ERR553403' with single-end layout -> to be discarded!

## assess representatitvity of first candidate list:
less download_firstroundlist.tsv \
| cut -f1 \
| grep -v -e 'ERR553395' -e 'ERR553403' \
| less \
| grep -w -F -f /dev/stdin dataset_joinedcollorstucki_treated.tsv \
| less \
| cut -f2 \
| sort \
| uniq -c \
| less \
| sort -nk2,2 \
| less \
| sed -e 's/^[ ]\+//g' -e 's/ /\t/g' \
| less \
>representativity_incollandstucki_firstlist.tsv ;
## 45 L1-L7 Coll classifications found on 9499/15038 overlap!!
### nicely representative list without oversampling. Might need to increase numbers for 2.1,4.1.3 and 4.3.1 later...
less representativity_incollandstucki_firstlist.tsv \
| awk '{print $2}' \
| awk 'NR==FNR {typingfound[$1] ; next} !($1 in typingfound)' /dev/stdin <(cut -f1 typingscheme_collandstucki.tsv | less) \
| less \
>representativity_missing_incollandstucki_firstlist.tsv ;
## similar situation as above, probably in the same groups! many L4 4-digit groups missing (and also 2 in L2)...
less download_firstroundlist.tsv \
| cut -f1 \
| grep -v -e 'ERR553395' -e 'ERR553403' \
| less \
| grep -w -F -f /dev/stdin dataset_tbprofiler2020.tsv \
| less \
| cut -f3 \
| sort \
| uniq -c \
| less \
| sort -nk2,2 \
| less \
| sed -e 's/^[ ]\+//g' -e 's/ /\t/g' \
| less \
>representativity_intbprofiler_firstlist.tsv ;
cat -n representativity_intbprofiler_firstlist.tsv ;
## nice representation on TBprofiler2020 typing scheme (97 classifications found in 15019/15699 overlap!).
### some L4 groups with low numbers but the rest seems to be OK!
less representativity_intbprofiler_firstlist.tsv \
| awk '{print $2}' \
| less \
| grep -v -w -F -f /dev/stdin <(cut -f4 typingscheme_tbprofiler.bed | sed -e 's/lineage//g' | sort -u | less) \
| less \
>representativity_missing_intbprofiler_firstlist.tsv ;
cat -n representativity_missing_intbprofiler_firstlist.tsv ;
## only 2.2 seems to be missing (apart from L7-L9, Bovis, Caprae and Orygis)!


echo "Try to enrich nonexistent TBprofiler groups or groups with low numbers" ;
#---------------------------------------------------------------------------------------
cat representativity_incollandstucki_firstlist.tsv <(awk '{print 0,$1}' OFS='\t' representativity_missing_incollandstucki_firstlist.tsv) \
| awk -F'\t' '{print $0,"collstucki"}' OFS='\t' \
| less \
| cat /dev/stdin <(cat representativity_intbprofiler_firstlist.tsv <(awk '{print 0,$1}' OFS='\t' representativity_missing_intbprofiler_firstlist.tsv) | awk -F'\t' '{print $0,"tbprofiler"}' OFS='\t' | less) \
| less \
| sort -nk1,1 \
| less \
>representativity_incollandtbprofiler_firstlist.tsv ;
cat -n representativity_incollandtbprofiler_firstlist.tsv ;

less representativity_incollandtbprofiler_firstlist.tsv \
| awk -F'\t' '$1<20 && $2<5 && length($2)>1' OFS='\t' \
| less \
| grep -v 'coll' \
| less \
| awk -F'\t' 'NR==FNR {typing[$2] ; next} ($3 in typing)' OFS='\t' /dev/stdin dataset_tbprofiler2020.tsv \
| less \
| cat /dev/stdin <(awk -F'\t' '$3==7' OFS='\t' dataset_tbprofiler2020.tsv) \
| less \
| awk -F'\t' 'NR==FNR {singlegenome[$1] ; next} !($1 in singlegenome)' OFS='\t' <(awk -F',' 'NR>1 {print $1}' single-endgenomes_mtb_ncbi.csv | sed -e '/^$/d' | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {mixture[$1] ; next} !($1 in mixture)' OFS='\t' <(less dataset_joinedcollorstucki_treated.tsv | awk -F'\t' '$2=="mixture"' OFS='\t' | less | cut -f1 | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {indownloadlist[$1] ; next} !($1 in indownloadlist)' OFS='\t' download_firstroundlist.tsv /dev/stdin \
| less \
| awk -F'\t' '{$1=$1 FS "https://sra-pub-run-odp.s3.amazonaws.com/sra/"$1"/"$1 ; print $0}' OFS='\t' \
| less \
>enrichment_withtbprofiler_firstlist.tsv ;
cat -n enrichment_withtbprofiler_firstlist.tsv ;
## 592 additional genomes got from tbprofiler2020!!
less enrichment_withtbprofiler_firstlist.tsv \
| cut -f4 \
| sort \
| uniq -c \
| less \
| sort -nk1,1 ;
## there are still some poorly represented groups (2.2, 4.3.4, 1.2 and 4.6.2)!!
less dataset_joinedcollorstucki_treated.tsv \
| awk -F'\t' '$2=="2.2" || $2=="4.3.4" || $2=="1.2" || $2=="4.6.2"' OFS='\t' \
| less \
| awk -F'\t' '$2=="2.2" || $2=="1.2"' OFS='\t' \
| less \
| grep -v -e 'chiner' -e 'pepperell' \
| less \
| awk -F'\t' 'NR==FNR {singlegenome[$1] ; next} !($1 in singlegenome)' OFS='\t' <(awk -F',' 'NR>1 {print $1}' single-endgenomes_mtb_ncbi.csv | sed -e '/^$/d' | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {mixture[$1] ; next} !($1 in mixture)' OFS='\t' <(less dataset_joinedcollorstucki_treated.tsv | awk -F'\t' '$2=="mixture"' OFS='\t' | less | cut -f1 | less) /dev/stdin \
| less \
| awk -F'\t' '{$1=$1 FS "https://sra-pub-run-odp.s3.amazonaws.com/sra/"$1"/"$1 ; print $0}' OFS='\t' \
| less \
>enrichment_withholt2018global_firstlist.tsv ;
cat -n enrichment_withholt2018global_firstlist.tsv ;
## 16 additional genomes got from holt2018 (global), 15 for 1.2 and 1 for 2.2.
### other groups seem to be very well represented in chineroms2019 dataset!
### 1.2 and 2.2 groups seem to be well represented in freschi2020_highquality (according to supplementary data and not found in the global dataset)...


echo "Enrich isolates from sublineages/internal groups highlighted in Freschi2020 Supplementary docx File 2" ;
#---------------------------------------------------------------------------------------
## manually extract the Supplementary Tables 3 and 4 (info for geographically restricted and unrestricted sublineages) from the Supplementary Information doc file (https://www.biorxiv.org/content/biorxiv/early/2020/09/29/2020.09.29.293274/DC1/embed/media-1.docx?download=true) and saved as 'dataset_freschi2020_interestgroups_geographicinfoandcounts.tsv'
less dataset_freschi2020_highquality_L1toL4counts.tsv \
| awk -F'\t' 'NR==FNR {countsbefore[$3]=$2 ; coll[$3]=$4 ; next} {print $1,countsbefore[$1],$3,coll[$1],$NF}' OFS='\t' /dev/stdin <(less dataset_freschi2020_interestgroups_geographicinfoandcounts.tsv | sed '1d') \
| sed '1iFreschityping\tCountsbefore\tCountslargerset\tColltyping\tNotes' \
| less \
>dataset_freschi2020_interestgroups_countsbeforeandlargerset.tsv ;
## 22 sublineages/internal groups of interest!

# get number of isolates of freschi2020 interest groups in all sets/typing schemes:
#------------------------------
less dataset_freschi2020_interestgroups_countsbeforeandlargerset.tsv \
| sed -e '1d' -e 's/,/\./g' -e 's/Corresp. to 4.4.1.1/4.4.1.1\tCorresp. to 4.4.1.1/g' -e 's/Internal group of 4.1\//4.1.2\tInternal group of 4.1/g' \
| awk -F'\t' 'NR==FNR {collcounts[$2]=$1 ; next} {print collcounts[$(NF-1)],$0}' OFS='\t' representativity_incollandstucki_firstlist.tsv /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {tbprofilercounts[$2]=$1 ; next} {print tbprofilercounts[$(NF-1)],$0}' OFS='\t' representativity_intbprofiler_firstlist.tsv /dev/stdin \
| less \
| cat <(head -1 dataset_freschi2020_interestgroups_countsbeforeandlargerset.tsv | sed -e 's/^/TBprofilercounts\tCollandstuckicounts\t/g') /dev/stdin \
| less \
>representativity_inallsets_freschi2020interestgroups.tsv ;

less representativity_inallsets_freschi2020interestgroups.tsv \
| awk -F'\t' '$1<20 || $2<20 || $4<20' OFS='\t' \
| cat -n
## 11 subgroups worth checking!
less representativity_inallsets_freschi2020interestgroups.tsv \
| awk -F'\t' '$1<20 && $2<20 && $4<20' OFS='\t' \
| cat -n ;
## comparing all the sets, there are 2 groups of interest with (apparent) low number of isolates -> 4.2.1.1.1.1.2 and 2.1!
### 4.2.1.1.1.2.2.1.1.1.2 corresponds to 4.6.1/Uganda and will be also added!


# get groups to enrich from freschi2020_largerset:
#------------------------------
less dataset_freschi2020_largerset.biosample \
| awk -F'\t' '$2=="4.2.1.1.1.1.2" || $2=="2.1" || $2=="4.2.1.1.1.2.2.1.1.1.2"' OFS='\t' \
| less \
| cut -f1 \
| less \
| grep -w -F -f /dev/stdin <(cut -d',' -f1,26 paired-endgenomes_mtb_ncbi.csv | sed -e 's/,/\t/g' | less) \
| less \
| awk -F'\t' 'NR==FNR {mixture[$1] ; next} !($1 in mixture)' OFS='\t' <(less dataset_joinedcollorstucki_treated.tsv | awk -F'\t' '$2=="mixture"' OFS='\t' | less | cut -f1 | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {indownloadlist[$1] ; next} !($1 in indownloadlist)' OFS='\t' <(cat download_firstroundlist.tsv enrichment_withtbprofiler_firstlist.tsv enrichment_withholt2018global_firstlist.tsv) /dev/stdin \
| less \
| awk -F'\t' '{$1=$1 FS "https://sra-pub-run-odp.s3.amazonaws.com/sra/"$1"/"$1 ; print $0}' OFS='\t' \
| less \
>enrichment_withfreschi2020largerset_firstlist.tsv ;
cat -n enrichment_withfreschi2020largerset_firstlist.tsv ;
## 111/191 additional run ids got from freschi2020 larger set!

# join all enrichment lists:
#------------------------------
cat enrichment_withtbprofiler_firstlist.tsv enrichment_withholt2018global_firstlist.tsv enrichment_withfreschi2020largerset_firstlist.tsv \
| less \
>enrichment_withallsets_firstlist.tsv ;
wc -l enrichment_withallsets_firstlist.tsv ;
## 719 additional run ids in the enrichment lists!
less paired-endgenomes_mtb_ncbi.csv \
| sed -e '1d' -e 's/,.*//g' -e '/^$/d' \
| cut -f1 \
| sort -u \
| less \
| grep -w -F -f /dev/stdin enrichment_withallsets_firstlist.tsv -c ;
## all 719 genomes found in paired-end list!


echo "Get biosamples for all run ids" ;
#---------------------------------------------------------------------------------------
cat download_firstroundlist.tsv enrichment_withallsets_firstlist.tsv \
| grep -v -e 'ERR553395' -e 'ERR553403' \
| less \
| cut -f1 \
| less \
| sort -u \
| less \
| grep -w -F -f /dev/stdin paired-endgenomes_mtb_ncbi.csv \
| awk -F',' '{print $1,$26}' OFS='\t' \
| less \
| datamash -s -g 2 unique 1 countunique 1 \
| less \
>dataset_biosamplecounts_pairedendmatch.tsv ;
head -2 dataset_biosamplecounts_pairedendmatch.tsv ;
wc -l dataset_biosamplecounts_pairedendmatch.tsv ;
## 16392/16409 unique run ids found in paired-end list of NCBI!
### 13083 biosamples found!

# get biosamples for not found run ids:
cat download_firstroundlist.tsv enrichment_withallsets_firstlist.tsv \
| grep -v -e 'ERR553395' -e 'ERR553403' \
| less \
| cut -f1 \
| less \
| sort -u \
| less \
| grep -v -w -F -f <(cut -f2 dataset_biosamplecounts_pairedendmatch.tsv | tr "," "\n" | less) /dev/stdin \
| less \
| while read runid ;
  do wget --quiet https://www.ncbi.nlm.nih.gov/sra/?term="$runid" -O - \
    | grep 'Link to BioSample.*' -o \
    | sed -e 's/Link to BioSample">//g' -e 's/<.*//g' \
    | awk -v runid="$runid" '{print runid,$1}' OFS='\t' ;
done \
| less \
| datamash -s -g 2 unique 1 countunique 1 \
| less \
>>dataset_biosamplecounts_pairedendmatch.tsv ;
wc -l dataset_biosamplecounts_pairedendmatch.tsv ;
## 13100 biosamples/isolates in the final list!!
## 17/17 missing genomes were added to the final dataset list (manually confirmed to belong to Mtb studies and to belong to a single Biosample)!


echo "Investigate biosample with multiple SRA runs" ;
#---------------------------------------------------------------------------------------
## collapse biosample IDs to paste into SRA Run Selector!
less dataset_biosamplecounts_pairedendmatch.tsv \
| awk -F'\t' '$NF>1 {print $1}' OFS='\t' \
| less \
| awk -F',' 'ORS=NR%250?FS:RS' \
| less \
>dataset_final_biosamplewithmultipleruns.txt ;
cat -n dataset_final_biosamplewithmultipleruns.txt ;
## 1678 biosamples collapsed into 7 rows!
### paste every row into search box at https://www.ncbi.nlm.nih.gov/Traces/study/? (there seems to be a hard limit of at least 1000 runs to return) -> saved as 'dataset_final_biosamplewithmultipleruns_part[1-7].csv'

## join and check downloaded metadata:
cat dataset_final_biosamplewithmultipleruns_part[1-7].csv \
| less \
| grep 'SAM[A-Z].*[0-9]\+,' -o \
| less \
| cut -d',' -f1 \
| sort -u \
| less \
| grep -w -F -f <(less dataset_biosamplecounts_pairedendmatch.tsv | awk -F'\t' '$NF>1 {print $1}' OFS='\t' | less) /dev/stdin -c ;
### all biosamples (n=1678) fetched!

## for each biosample, get SRA run with highest number of bases:
cat dataset_final_biosamplewithmultipleruns_part[1-7].csv \
| less \
| cut -d',' -f1-10 \
| less \
| sed -e '/^Run/d' -e 's/,/\t/g' \
| less \
| awk -F'\t' '{for(i=1;i<=NF;i++){if($i ~ /^SAM/ ) print $1,$i,$(i-2),$(i+1)}}' OFS='\t' \
| less \
| datamash --full -s -g 2 max 3 \
| less \
| awk -F'\t' '{print $0,"https://sra-pub-run-odp.s3.amazonaws.com/sra/"$1"/"$1}' OFS='\t' \
| less \
>dataset_final_biosamplewithmultipleruns_maxbases.tsv ;
head -2 dataset_final_biosamplewithmultipleruns_maxbases.tsv ;
wc -l dataset_final_biosamplewithmultipleruns_maxbases.tsv ;
## 1678 run ids -> 1 representative of each Biosample!
### validate datamash --full:
less dataset_final_biosamplewithmultipleruns_maxbases.tsv \
| awk -F'\t' 'NR==FNR {maxbases[$1]=$(NF-1) ; next} ($1 in maxbases){for(i=1;i<=10;i++){if($i==maxbases[$1]) print $1,$i}}' OFS='\t' /dev/stdin <(cat dataset_final_biosamplewithmultipleruns_part[1-7].csv | less | sed -e 's/,/\t/g' | less) \
| less ;
#### datamash seems to be validated!


echo "Build final list of run ids to download" ;
#---------------------------------------------------------------------------------------  
less dataset_biosamplecounts_pairedendmatch.tsv \
| awk -F'\t' '$NF>1 {print $2}' \
| less \
| tr "," "\n" \
| awk '{print $0}' \
| less \
| awk -F'\t' 'NR==FNR {runidsofbiosamplesmultipleruns[$1] ; next} !($1 in runidsofbiosamplesmultipleruns)' OFS='\t' /dev/stdin <(cat download_firstroundlist.tsv enrichment_withallsets_firstlist.tsv | grep -v -e 'ERR553395' -e 'ERR553403' | less) \
| less \
| cat /dev/stdin <(less dataset_final_biosamplewithmultipleruns_maxbases.tsv | awk -F'\t' '{print $1,$NF}' OFS='\t') \
| less \
| cut -f1,2 \
| sort -u \
| less \
>downloadlist_final.tsv ;
tail -2 downloadlist_final.tsv ;
wc -l downloadlist_final.tsv ;
## 13100 run ids to download!!
