
mkdir -p ../NGS_analysis/Lineage_typing ;
cd ../NGS_analysis/Lineage_typing ;


echo "Extract lineage typing info from VCF files" ;
#---------------------------------------------------------------------------------------
## get typing info with and without problematic filters
i=0 ;
total_genomes=$(less ../listofgenomes_forpipeline.txt | wc -l) ;

time for sample in $(cat ../listofgenomes_forpipeline.txt) ;
  do ((++i)) ; echo $i"/"$total_genomes ;
  
    echo sample="$sample" ;
    vcf=$(echo "$sample"  | awk '{print "../"$1"_bcftools_varsonly_annotated_withfilters.vcf.gz"}') ;
  
    
    echo "Get lineage/sublineage typing from unfiltered VCF files" ;
    #---------------------------------------------------------------------------------------
    ## adds also missing 4.10 sublineage tag at POS=1692141
    bcftools query -f'[%POS\t%REF\t%ALT\t%FILTER\t%Lineage_tag\n]' --include 'Lineage_tag!="" || POS=1692141' "$vcf" \
    | less \
    | awk -F'\t' '{if($1==1692141 && $2=="C" && $3=="A"){$NF="!lineage,4.10,mykrobe" ; print $0} else print $0}' OFS='\t' \
    | less \
    | awk -F'\t' 'NR==FNR {FILTERofposrefalt[$1 FS $2 FS $3]=$4 ; next} ($1 FS $2 FS $3 in FILTERofposrefalt){print $0,FILTERofposrefalt[$1 FS $2 FS $3]}' OFS='\t' /dev/stdin <(find $HOME/Code/NGS_helper_files/ -name "lineagesnps*.tsv" | less | grep -v -e 'original' -e 'joined' | less | xargs cat | sed -e 's/1692141\tA\tC/1692141\tC\tA/g' | less) \
    | less \
    | awk -F'\t' '{print $1"|"$2"|"$3,$4"!"$5"!"length($5),$NF}' OFS='\t' \
    | less \
    | datamash -s -g 2 unique 1,3 \
    | less \
    | sed -e 's/!/\t/g' \
    >"$sample"_lineagetypingfromvcf.tsv ;

    
    echo "Get lineage/sublineage typing, excluding sites on VCFS with problematic filters" ;
    #---------------------------------------------------------------------------------------
    minimalproblematicsites=$(grep -e 'RLC' -e 'LowMQ40' -e 'LowVAF' -e 'EBR90' -e 'Mappability' "$sample"_lineagetypingfromvcf.tsv) ;

    if [ -n "$minimalproblematicsites" ] ;
      then echo "$minimalproblematicsites" \
        | grep -v -w -F -f /dev/stdin "$sample"_lineagetypingfromvcf.tsv \
        >"$sample"_lineagetyping_excludedlocifromvcf.tsv ;
    fi ;


    echo "Assign a single typing for all schemes, raw and filtered VCF files" ;
    #---------------------------------------------------------------------------------------
    ## for each typing scheme (mykrobe,napier2020,tbprofiler,coscolla2021 and freschi2020), get the lowest typing level (e.g. 2.2.1 instead of 2 or 2.2)
    ### if different lineages or sublineages are found, assign as "mixed"
    for file in $(ls "$sample"_lineagetyping*.tsv) ;
      do outfile=$(echo "$file" | sed -e 's/_lineagetypingfromvcf.tsv/_lineagetyping.tsv/g' -e 's/excludedlocifromvcf/excludedloci/g') ;

        less "$file" \
        | datamash -s -g 1 collapse 2 \
        | less \
        | awk -F'\t' '{split($NF,lineage,",") ; nlineages=1 ; for(i=2;i<=length(lineage);i++){previouslevel=lineage[i-1] ; currentlevel=substr(lineage[i],1,length(previouslevel)) ; if(previouslevel!=currentlevel) nlineages++} print $0,nlineages,i}' OFS='\t' \
        | less \
        | awk -F'\t' '{if($(NF-1)>1) print $1,"mixed" ; else {split($2,lineage,",") ; print $1,lineage[length(lineage)]}}' OFS='\t' \
        | less \
        | awk -F'\t' 'NR==FNR {lineage[$1]=$2 ; next} {for(i=1;i<=NF;i++){if(lineage[$i]=="") print "---" ; else print lineage[$i]}}' ORS='\t' /dev/stdin <(echo -e mykrobe"\t"napier2020"\t"tbprofiler"\t"coscolla2021"\t"freschi2020) \
        | awk -F'\t' '{print $0}' OFS='\t' \
        | less \
        | sed -e "s/^/$sample\t/g" -e 's/\t$//g' \
        | less \
        >"$outfile" ;
    done ;
done ;
## 10min12s!


echo "Join QC and typing files" ;
#---------------------------------------------------------------------------------------
## get Kraken2 reports and coverage metrics
## join raw and filtered (minimalproblematicsites) typing files
### some strains have all sites as PASS (e.g. ERR017794), so typing is set as equal to raw VCF
## for each typing scheme, check differences between raw and filtered (all levels: "Diffsalleach" or just lineage level: "DiffLeach")
## check differences between schemes (except coscolla2021) at the lineage level (raw VCF only)
time cat ../*mtbcreadcheck.tsv \
| less \
| awk -F'\t' 'NR==FNR {coverage[$1]=$2 FS $3 ; next} {print $0,coverage[$1]}' OFS='\t' <(cat ../*coveragecheck.tsv | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {lineage[$1]=$2 FS $3 FS $4 FS $5 FS $6 ; next} {print $0,lineage[$1]}' OFS='\t' <(cat *_lineagetyping.tsv | less) /dev/stdin \
| less \
| awk -F'\t' 'NR==FNR {lineagefiltered[$1]=$2 FS $3 FS $4 FS $5 FS $6 ; next} {if(lineagefiltered[$1]==""){lineagefiltered[$1]=$7 FS $8 FS $9 FS $10 FS $11} ; print $0,lineagefiltered[$1]}' OFS='\t' <(cat *_lineagetyping_excludedloci.tsv | less) /dev/stdin \
| less \
| sort -k1,1 \
| sed '1iStrain\tMTBC%\tUnk%\tNonmtbc%\tCov_mean\tCov_h10\tmykrobe\tnapier2020\ttbprofiler\tcoscolla2021\tfreschi2020\tfilter_mykrobe\tfilter_napier2020\tfilter_tbprofiler\tfilter_coscolla2021\tfilter_freschi2020' \
| less \
| awk -F'\t' 'NR==1 {split($0,header,FS) ; print $0,"Diffsalleach","DiffLeach","Lineages"} NR>1 {lineagesarray[1]="!" ; lineage="" ; lineagefiltered="" ; diffsall="" ; difflineage="" ; for(i=7;i<=11;i++){ split($i,typing,".") ; split($(i+5),typingfiltered,".") ; lineage=typing[1] ; lineagefiltered=typingfiltered[1] ; if(lineage=="" || lineage=="---" || i==10){lineage="discard"} ; if($i!=$(i+5) && $(i+5)!="" && $i!="discard"){diffsall=diffsall","header[i] ; if(lineagefiltered!=lineage){difflineage=difflineage","header[i]} } if(lineage!="discard" && lineage!=lineagesarray[length(lineagesarray)]){lineagesarray[length(lineagesarray)+1]=lineage} } if(length(lineagesarray)>1){for(j=2;j<=length(lineagesarray);j++){ if(lineagesarray[j]!=lineagesarray[j-1] && lineagesarray[j-1]!="!"){lineagesall=lineagesarray[j-1]","lineagesarray[j]} else lineagesall=lineagesarray[2] }} ; print $0,diffsall,difflineage,lineagesall ; delete lineagesarray ; lineagesall=""}' OFS='\t' \
| less \
| sed -e 's/\t,/\t/g' \
| less \
| awk -F'\t' 'NF>15 {print $0} NF<19 {print $0,"","","","noinfo"}' OFS='\t' \
| less \
>qcandlineage_info.tsv ;
## ≃0.4s!
less qcandlineage_info.tsv \
| wc -l ;
## 13605 rows, confirmed!
awk -F'\t' '{print NF}' qcandlineage_info.tsv \
| sort -u ;
## all rows with 19 fields, confirmed!


echo "Select samples with appropriate QC metrics and lineage typing" ;
#---------------------------------------------------------------------------------------
## select genomes with: 
### >=90% MTBC DNA ; 
### covered by >=10 mapped reads in >=95% of the reference genome length ;
### a single lineage typing (L1 to L7) 
less qcandlineage_info.tsv \
| grep -v -e 'mixed' -e 'Animal' \
| awk -F'\t' 'NR>1 && $2>=90 && $6>=0.95 && length($NF)==1 {print $1,"L"$NF}' OFS='\t' \
| less \
>validsamples.tsv ;
head -2 validsamples.tsv ;
wc -l validsamples.tsv ;
## 11580 strains!


echo "Get number of isolates per lineage"
#---------------------------------------------------------------------------------------  
less validsamples.tsv \
| datamash -s -g 2 countunique 1 \
| less \
>lineage_counts.tsv ;
cat -n lineage_counts.tsv ;


echo "Compress reports of Kraken2, coverage metrics and lineage typing" ;
#---------------------------------------------------------------------------------------
tar -cvzf ../Kraken2_reports.tar.gz ../*mtbcreadcheck.tsv ;
tar -cvzf ../coverage_metrics.tar.gz ../*coveragecheck.tsv ;
du -shc ../*.tar.gz ;
## 2 archives, 512KB!
rm ../*mtbcreadcheck.tsv ../*coveragecheck.tsv ;

tar -cvzf lineagetyping_reports.tar.gz *lineagetyping*.tsv ;
ls -ltrh | tail -2 ;
## 1.4MB archive!
rm *lineagetyping*.tsv ;
