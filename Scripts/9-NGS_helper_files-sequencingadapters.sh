
cd ../NGS_helper_files ;

echo "Get Stephen Turner's combined adapter list of Trimmomatic, bbmap, FastQC and others" ;
#---------------------------------------------------------------------------------------
##https://github.com/OpenGene/fastp/issues/58#issuecomment-459593594
##https://github.com/stephenturner/adapters
wget "https://github.com/stephenturner/adapters/raw/master/adapters_combined_256_unique.fasta" ;
cat -n adapters_combined_256_unique.fasta ;
## 256 adapter sequences!

ls -ltrh ;

