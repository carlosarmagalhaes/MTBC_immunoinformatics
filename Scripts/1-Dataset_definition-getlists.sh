mkdir -p ../Dataset_definition ;
cd ../Dataset_definition ;

echo "Retrieve and convert lists of accession numbers" ;
#---------------------------------------------------------------------------------------
## retain at least accession numbers (SRA and Biosample) and typing info (when available)

# Napier 2020:
wget "https://github.com/GaryNapier/tb-lineages/blob/main/samples_github.csv?raw=true" -O dataset_tbprofiler2020.csv ;

less dataset_tbprofiler2020.csv \
| awk -F',' 'NR>1 {print $1,$2,"tbprofiler2020"}' OFS='\t' \
| less \
| awk -F'\t' '{split($1,sraid,"_") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0}' OFS='\t' \
| less \
>dataset_tbprofiler2020.tsv ;
awk -F'\t' '{print NF}' dataset_tbprofiler2020.tsv \
| sort -u ;
## same number of fields
## some samples have multiple run accessions (separated by "_", it is clear on row 3855 that those are multiple run ids and not ranges)...

# Stucki 2016:
wget "https://static-content.springer.com/esm/art%3A10.1038%2Fng.3704/MediaObjects/41588_2016_BFng3704_MOESM65_ESM.xlsx" -O dataset_stucki2016.xlsx ;
xlsx2csv_bin=$(find $HOME/Software/anaconda3/envs/htslib19_py27/ -name "xlsx2csv") ;
echo "$xlsx2csv_bin" ;
"$xlsx2csv_bin" dataset_stucki2016.xlsx dataset_stucki2016.csv ;

less dataset_stucki2016.csv \
| sed -e 's/, /!/g' \
| less \
| awk -F',' 'NR>1 {print $5,$NF,"stucki2016"}' OFS='\t' \
| less \
| awk -F'\t' '$1!="" && $2!="" {split($1,sraid,";") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0}' OFS='\t' \
| less \
>dataset_stucki2016.tsv ;
## 123 strains lost due to no run accession number available (does not seem to decrease representativity of the dataset)...
## some samples have multiple run accessions...
awk -F'\t' '{print NF}' dataset_stucki2016.tsv \
| sort -u ;
## same number of fields

# Brynildsrud 2018:
wget "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6192687/bin/aat5869_Dataset_S1.xls" -O dataset_brynildsrud2018.xls ;
## manually saved to 'dataset_brynildsrud2018.csv' with LibreOffice Calc (error on xlsx2csv must have been caused by accentuation...)

less dataset_brynildsrud2018.csv \
| awk -F',' 'NR>3 && NR<1671 && $3=="" {print $5,$(NF-3)"."$(NF-2),"brynildsrud2018"}' OFS='\t' \
| less \
| sed -e 's/"//g' \
| less \
| awk -F'\t' '$1!="-"' OFS='\t' \
| less \
>dataset_brynildsrud2018.tsv ;
## 2 strains lost due to no SRA ID availabilty...
awk -F'\t' '{print NF}' dataset_brynildsrud2018.tsv \
| sort -u ;
## same number of fields

# O'Neill/Pepperell 2019:
wget "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6660993/bin/MEC-28-3241-s003.xlsx" -O dataset_pepperell2019.xlsx ;
"$xlsx2csv_bin" dataset_pepperell2019.xlsx dataset_pepperell2019.csv ;

less dataset_pepperell2019.csv \
| grep -v -e '"' -e 'multiple genome alignment' \
| less \
| awk -F',' 'NR>3 {print $2,$9}' OFS='\t' \
| less \
| cat /dev/stdin <(grep '"' dataset_pepperell2019.csv | less | grep -v 'multiple genome alignment' | less | awk -F'"' '{gsub(",",";",$2) ; print $0}' OFS=',' | less | cut -d',' -f3,11 | sed -e 's/,/\t/g' | less) \
| less \
| awk -F'\t' '{split($1,sraid,";") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0,"pepperell2019"}' OFS='\t' \
| less \
>dataset_pepperell2019.tsv ;
## 4 strains without lineage info!
## some samples have multiple run accessions...
awk -F'\t' '{print NF}' dataset_pepperell2019.tsv \
| sort -u ;
## same number of fields

# Chiner-Oms 2019:
## manually extracted Table S7 from https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6691555/bin/aaw3307_Tables_S1_to_S8.xlsx
less dataset_chineroms2019.csv \
| awk -F'\t' 'NR>3 {print $1,$2,"chineroms2019"}' OFS='\t' \
| less \
| sed -e 's/lineage//g' -e '/animal/d' \
| less \
>dataset_chineroms2019.tsv ;
## 78 strains flagged as "possible co-infection" will need to be taken into account later on!
### 3 L6(animal) strains excluded
## samples with possible mixture of strains might be useful to discard samples in the final list!

# molecular clock plospathogens 2019:
wget "https://doi.org/10.1371/journal.ppat.1008067.s004" -O dataset_molecularclock2019_unfiltered.xlsx ;
wget "https://doi.org/10.1371/journal.ppat.1008067.s001" -O dataset_molecularclock2019_filtered.xlsx ;
"$xlsx2csv_bin" dataset_molecularclock2019_unfiltered.xlsx dataset_molecularclock2019_unfiltered.csv ;
"$xlsx2csv_bin" dataset_molecularclock2019_filtered.xlsx dataset_molecularclock2019_filtered.csv ;
less dataset_molecularclock2019_unfiltered.csv ;
less dataset_molecularclock2019_filtered.csv ;

less dataset_molecularclock2019_unfiltered.csv \
| sed '1d' \
>dataset_molecularclock2019_unfiltered.biosample ;
## only biosample IDs provided!
less dataset_molecularclock2019_filtered.csv \
| sed -e 's/[,]\+/,/g' -e 's/,$//g' \
| less \
| awk -F',' 'NR>1 {print $3,$4";"$5";"$6";"$7";"$8}' OFS='\t' \
| less \
| sed -e 's/[;]\+/;/g' -e 's/;$//g' \
| less \
| awk -F'\t' '{print $2,$1,"mclock2019filtered"}' OFS='\t' \
| less \
| awk -F'\t' '{split($1,sraid,";") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0}' OFS='\t' \
| less \
>dataset_molecularclock2019_filtered.tsv ;
## some samples have multiple run accessions...
### biosample IDs here can be used to filter out other datasets!
awk -F'\t' '{print NF}' dataset_molecularclock2019_filtered.tsv \
| sort -u ;
## same number of fields

# Furió/Iñaki 2020:
wget "https://static-content.springer.com/esm/art%3A10.1038%2Fs42003-021-02846-z/MediaObjects/42003_2021_2846_MOESM6_ESM.csv" -O dataset_furio2020.csv ;

less dataset_furio2020.csv \
| awk '$1!="NG-5604" {print $0,"furio2020"}' OFS='\t' \
| less \
>dataset_furio2020.tsv ;
awk -F'\t' '{print NF}' dataset_furio2020.tsv \
| sort -u ;
## same number of fields

# Freschi 2020:
wget "https://www.biorxiv.org/content/biorxiv/early/2020/09/29/2020.09.29.293274/DC2/embed/media-2.xlsx" -O dataset_freschi2020_highquality.xlsx ;
wget "https://www.biorxiv.org/content/biorxiv/early/2020/09/29/2020.09.29.293274/DC5/embed/media-5.xlsx" -O dataset_freschi2020_largerset.xlsx ;
"$xlsx2csv_bin" dataset_freschi2020_highquality.xlsx dataset_freschi2020_highquality.csv ;
"$xlsx2csv_bin" dataset_freschi2020_largerset.xlsx dataset_freschi2020_largerset.csv ;
less dataset_freschi2020_highquality.csv ;
less dataset_freschi2020_largerset.csv ;

less dataset_freschi2020_highquality.csv \
| grep '^SAM' \
| less \
| awk -F',' '{print $1,"freschi2020highquality"}' OFS='\t' \
| less \
>dataset_freschi2020_highquality.biosample ;
## only biosample IDs available!
## >1200 genomes excluded due to non-availability of biosample IDs (could not find the reason why or any NCBI ID in the paper for these strains)
awk -F'\t' '{print NF}' dataset_freschi2020_highquality.biosample \
| sort -u ;
## same number of fields

less dataset_freschi2020_largerset.csv \
| sed -e 's/patric,ncbi/patric;ncbi/g' \
| less \
| awk -F',' 'NR>1 {print $2,$NF,"freschi2020largerset"}' OFS='\t' \
| less \
>dataset_freschi2020_largerset.biosample ;
## only biosample IDs available!
## some samples do not have sublineage or any typing info...
awk -F'\t' '{print NF}' dataset_freschi2020_largerset.biosample \
| sort -u ;
## same number of fields

## manually extracted inputs from suppdata media3.xlsx:
wget "https://www.biorxiv.org/content/biorxiv/early/2020/09/29/2020.09.29.293274/DC3/embed/media-3.xlsx?download=true" -O dataset_freschi2020_highquality_L1toL4counts.xlsx ;
ls dataset_freschi2020_highquality_L[1-4]counts.csv \
| less \
| xargs cat \
| less \
| sed -e '/^node/d' -e '/^\t/d' -e 's/,/\./g' \
| less \
>dataset_freschi2020_highquality_L1toL4counts.tsv ;
awk -F'\t' '{print NF}' dataset_freschi2020_highquality_L1toL4counts.tsv \
| sort -u ;
## not the same number of fields but the ones of interest seem to be in the same position...

# Coscolla 2021:
wget "https://www.ncbi.nlm.nih.gov/pmc/articles/PMC8208692/bin/mgen-7-477-s001.xlsx" -O dataset_coscolla2021.xlsx ;
"$xlsx2csv_bin" dataset_coscolla2021.xlsx dataset_coscolla2021.csv ;

less dataset_coscolla2021.csv \
| sed -e 's/[,]\+/,/g' -e 's/,$//g' -e '/^$/d' \
| less \
| awk -F',' 'NR>2 {print $3,$12,$13,"coscolla2021"}' OFS='\t' \
| less \
| awk -F'\t' '{split($1,sraid,";") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0}' OFS='\t' \
| less \
>dataset_coscolla2021.tsv ;
## some samples have multiple run accessions...
awk -F'\t' '{print NF}' dataset_coscolla2021.tsv \
| sort -u ;
## same number of fields
less dataset_coscolla2021.tsv \
| grep 'X' ;
## a few isolates do not have the exact SRA ID (e.g. https://www.ncbi.nlm.nih.gov/sra/?term=SRX007726)

# Menardo 2021:
wget "https://github.com/fmenardo/MTBC_L1_L3/blob/main/Supplementary_text_figures_tables/Sup_Table1.xlsx?raw=true" -O dataset_menardo2021.xlsx ;
"$xlsx2csv_bin" dataset_menardo2021.xlsx dataset_menardo2021_L1L3.csv ;

less dataset_menardo2021_L1L3.csv \
| awk -F',' 'NR>1 {print $3,$2,$(NF-1),$NF,"menardo2021L1L3"}' OFS='\t' \
| less \
>dataset_menardo2021_L1L3.tsv ;
## some isolates do not have run ID, other do not have any ID (=NA)!
## biosamples with run IDs here can be used to find run IDs in other datasets!
## some samples have multiple run accessions...
awk -F'\t' '{print NF}' dataset_menardo2021_L1L3.tsv \
| sort -u ;
## same number of fields

## L2 and L4 counts manually extracted from dataset_menardo2021.xlsx
cat dataset_menardo2021_L2.csv dataset_menardo2021_L4.csv \
| less \
| awk -F',' '{if(NR>1 && NR<6754) print $NF,$2,"L2","L2","menardo2021L2" ; else if(NR>6754) print $NF,$2,"L4","L4","menardo2021L4"}' OFS='\t' \
| less \
| sed -e 's/"//g' \
| less \
>dataset_menardo2021_L2L4.tsv ;
## biosample + SRA IDs available!!
## some samples have multiple run accessions...
grep '^DRR' dataset_menardo2021_L2L4.tsv ;
## DRR isolates follow the same data access pattern! (https://trace.ncbi.nlm.nih.gov/Traces/sra/?run=DRR130198)
awk -F'\t' '{print NF}' dataset_menardo2021_L2L4.tsv \
| sort -u ;
## same number of fields
cat dataset_menardo2021_L1L3.tsv dataset_menardo2021_L2L4.tsv \
| less \
| awk -F'\t' '{split($1,sraid,";") ; for(i=1;i<=length(sraid);i++) print sraid[i],$0}' OFS='\t' \
| less \
>dataset_menardo2021_L1toL4.tsv ;
## menardo2021 dataset joined. Contains run id + biosample ID, which is very useful info!
awk -F'\t' '{print NF}' dataset_menardo2021_L1toL4.tsv \
| sort -u ;
## same number of fields

# Holt 2018:
wget "https://static-content.springer.com/esm/art%3A10.1038%2Fs41588-018-0117-9/MediaObjects/41588_2018_117_MOESM3_ESM.xlsx" -O dataset_holt2018_study.xlsx ;
wget "https://static-content.springer.com/esm/art%3A10.1038%2Fs41588-018-0117-9/MediaObjects/41588_2018_117_MOESM4_ESM.xlsx" -O dataset_holt2018_globalset.xlsx ;
"$xlsx2csv_bin" dataset_holt2018_study.xlsx dataset_holt2018_study.csv ;
"$xlsx2csv_bin" dataset_holt2018_globalset.xlsx dataset_holt2018_globalset.csv ;

less dataset_holt2018_study.csv \
| awk -F',' 'NR>1 {print $2,$3,$4,"holt2018study"}' OFS='\t' \
| less \
>dataset_holt2018_study.tsv ;
awk -F'\t' '{print NF}' dataset_holt2018_study.tsv \
| sort -u ;
## same number of fields

less dataset_holt2018_globalset.csv \
| sed -e 's/.*ERR/ERR/g' -e 's/.*SRR/SRR/g' -e 's/, Russia/|Russia/g' \
| awk -F',' 'NR>1 {print $1,$4,$5,"holt2018global"}' OFS='\t' \
| less \
>dataset_holt2018_globalset.tsv ;
awk -F'\t' '{print NF}' dataset_holt2018_globalset.tsv \
| sort -u ;
## same number of fields

# Palittapongarnpim 2018:
wget "https://static-content.springer.com/esm/art%3A10.1038%2Fs41598-018-29986-3/MediaObjects/41598_2018_29986_MOESM4_ESM.xlsx" -O dataset_thai2018.xlsx ;
"$xlsx2csv_bin" dataset_thai2018.xlsx dataset_thai2018.csv ;

less dataset_thai2018.csv \
| awk 'NR>1 {print $1,"thai2018"}' OFS='\t' \
| less \
>dataset_thai2018.tsv ;
awk -F'\t' '{print NF}' dataset_thai2018.tsv \
| sort -u ;
## same number of fields

# Liu 2021:
less dataset_tibete2021.txt \
| awk '{print $1,"tibete2021"}' OFS='\t' \
| less \
>dataset_tibete2021.tsv ;
## manually retrieved from https://www.ncbi.nlm.nih.gov/Traces/study/?query_key=2&WebEnv=MCID_6088742eb9a1e6301d28b071&o=acc_s%3Aa
awk -F'\t' '{print NF}' dataset_tibete2021.tsv \
| sort -u ;
## same number of fields

ls -ltrh ;
#---------------------------------------------------------------------------------------
