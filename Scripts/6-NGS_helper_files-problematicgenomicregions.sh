
cd ../NGS_helper_files ;


echo "Retrieve data" ;
#---------------------------------------------------------------------------------------
wget "https://github.com/farhat-lab/mtb-illumina-wgs-evaluation/raw/main/References/Mtb_H37Rv_MaskingSchemes/RLC_Regions.H37Rv.bed" -O excluded_loci_RLC2021_original.tsv ;
wget "https://www.biorxiv.org/content/biorxiv/early/2021/05/26/2021.04.08.438862/DC17/embed/media-17.tsv?download=true" -O blindspots_EBRscore_marin2021_original.tsv ;
wget "https://raw.githubusercontent.com/farhat-lab/mtb-illumina-wgs-evaluation/main/Results/C_BrowserTracks/201027_H37rv_PileupMappability_K50_E4.bedgraph" -O blindspots_mappability_marin2021_original.tsv ;

#------------------------------


echo "Create annotation files for RLCs, EBR and mappability scores" ;
#---------------------------------------------------------------------------------------
less excluded_loci_RLC2021_original.tsv \
| awk -F'\t' '{print "MTB_anc",$2,$3,"!RLC"}' OFS='\t' \
| sed '1i#CHROM\tFROM\tTO\tRLC_tag' \
| less \
>excludedloci_RLC2021.tab ;

less blindspots_EBRscore_marin2021_original.tsv \
| awk -F'\t' '$NF!="" {printf "%s\t%s\t%s\t%1.2g\n", "MTB_anc",$2+1,$3+1,$NF}' \
| less \
| sed '1i#CHROM\tFROM\tTO\tEBR' \
| less \
>blindspots_EBRscore_marin2021.tab ;

## https://unix.stackexchange.com/questions/131073/awk-printf-number-in-width-and-round-it-up ; https://www.gnu.org/software/gawk/manual/html_node/Printf-Examples.html
## https://genome.ucsc.edu/goldenPath/help/bedgraph.html
less blindspots_mappability_marin2021_original.tsv \
| awk -F'\t' '$NF!="" {printf "%s\t%s\t%s\t%1.2g\n", "MTB_anc",$2+1,$3+1,$NF}' \
| less \
| sed '1i#CHROM\tFROM\tTO\tMappability' \
| less \
>blindspots_mappability_marin2021.tab ;
## not entirely sure about bed coordinate conversion to 1-based scheme...

#------------------------------------------------------


echo "Create input for bcftools annotate: bgzip + tabix index + header" ;
#---------------------------------------------------------------------------------------
#http://www.htslib.org/doc/bcftools.html#annotate ; https://github.com/samtools/bcftools/wiki/HOWTOs#annotate-from-bed ;

ls *.tab \
| while read annotation ;
  do echo annotation="$annotation" ;
    
    namewithannotsuffix=$(echo "$annotation" | sed -e 's/\.tab/_annot.tab/g') ;
    echo namewithannotsuffix="$namewithannotsuffix" ;
    
    cp "$annotation" "$namewithannotsuffix" ;
    bgzip --force "$namewithannotsuffix" ;
    tabix --sequence 1 --begin 2 --end 3 --force "$namewithannotsuffix".gz ;

done ;
ls -ltrh ;

#------------------------------------------------------

#Create VCF headers:
echo '##INFO=<ID=RLC_tag,Number=.,Type=String,Description="Refined low confidence regions by Marin 2021">' \
| tee excludedloci_RLC2021_annot.header ;
##--merge-logic TAG:append returns error "can be requested only with tags of variable length Number=." if "Number=1"
echo '##INFO=<ID=Mappability,Number=.,Type=String,Description="Pileup mappability score (0-1) calculated by Marin 2021">' \
| tee blindspots_mappability_marin2021_annot.header ;
## Mappability should have been defined as Float -> https://www.biostars.org/p/315022/
echo '##INFO=<ID=EBR,Number=.,Type=Float,Description="Empirical base-level recall (0-1) calculated by Marin 2021, i.e. the proportion of isolates where Illumina WGS confidently and correctly agreed with the PacBio defined ground truth">' \
| tee blindspots_EBRscore_marin2021_annot.header ;

ls -ltrh | tail ;

#------------------------------------------------------


echo "Test annotation process of problematic regions" ;
#---------------------------------------------------------------------------------------
zcat ERR2653038_bcftools_varsonly.vcf.gz \
| bcftools annotate \
--annotations excludedloci_RLC2021_annot.tab.gz \
--header-lines excludedloci_RLC2021_annot.header \
--columns CHROM,FROM,TO,RLC_tag \
--threads 12 --output-type u \
| bcftools annotate \
--annotations blindspots_mappability_marin2021_annot.tab.gz \
--header-lines blindspots_mappability_marin2021_annot.header \
--columns CHROM,FROM,TO,Mappability \
--threads 12 --output-type u \
| bcftools annotate \
--annotations blindspots_EBRscore_marin2021_annot.tab.gz \
--header-lines blindspots_EBRscore_marin2021_annot.header \
--columns CHROM,FROM,TO,EBR \
--threads 12 --output-type u \
| bcftools query -f'[%CHROM\t%POS\t%RLC_tag\t%Blindspot\t%Mappability\t%EBR\n]' \
| less \
| awk -F'\t' '$3!="." || $4!="." || $5!="." || $6!="."' OFS='\t' \
| cat -n ;
## regions successfully highlighted!

