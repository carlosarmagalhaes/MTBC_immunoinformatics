
mkdir -p ../NGS_helper_files ;
cd ../NGS_helper_files ;


echo "Download and unwrap reference genome" ;
#---------------------------------------------------------------------------------------
wget "https://zenodo.org/records/3497110/files/MTB_ancestor_reference.fasta?download=1" -O - \
| less \
| sed '1d' \
| tr "\n" "," \
| awk '{print $0}' \
| sed -e '1i>MTB_anc' -e 's/,//g' \
| less \
>MTB_anc.fasta ;


echo "Index reference genome for use with bwa and others" ;
#---------------------------------------------------------------------------------------
bwa index MTB_anc.fasta ;
samtools faidx MTB_anc.fasta ;
ls -ltrh ;


