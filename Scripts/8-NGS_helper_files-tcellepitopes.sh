
cd ../NGS_helper_files ;


echo "Get known T cell epitopes from Menardo 2021 paper" ;
#---------------------------------------------------------------------------------------
wget "https://raw.githubusercontent.com/fmenardo/MTBC_L1_L3/main/Supplementary_text_figures_tables/Sup_Table7.xlsx" -O iedbepitopes_menardo2021_original.xlsx ;
## sheets were manually extracted and saved as 'iedbepitopes_mhc[1-2]_menardo2021_original.tsv'

less iedbepitopes_mhc1_menardo2021_original.tsv \
| awk -F'\t' 'NR>1 {print "MTB_anc",$4,$5,"!IEDBMHC1,"$1,$0}' OFS='\t' \
| less \
| cat /dev/stdin <(less iedbepitopes_mhc2_menardo2021_original.tsv | awk -F'\t' 'NR>1 {print "MTB_anc",$4,$5,"!IEDBMHC2,"$1,$0}' OFS='\t' | less) \
| less \
>iedbepitopes_all_menardo2021.tsv ;

grep -w 'AAAGTAVQDSRSHVYAHQAQ' iedbepitopes_all_menardo2021.tsv ;
## there are duplicated peptides in menardo2021! It is because the same peptide can be simultaneously presented by MHC1 and MHC2 proteins! (at least in this example peptide)


echo "Get known T cell epitopes from Vargas 2021 paper" ;
#---------------------------------------------------------------------------------------
wget "https://cdn.elifesciences.org/articles/61805/elife-61805-supp11-v2.csv" -O iedbepitopes_vargas2021_original.csv ;
wc -l iedbepitopes_vargas2021_original.csv ;
## 1875 rows. Seems to have more epitopes than in Menardo 2021 paper. Let's inspect this...

# Are there epitopes or epitope locations in Vargas 2021 but not present in Menardo 2021?
uniqueepitopesvargas=$(less iedbepitopes_all_menardo2021.tsv \
| cut -f7 \
| less \
| grep -v -w -F -f /dev/stdin <(less iedbepitopes_vargas2021_original.csv | sed -e 's/,/\t/g' -e '1d' | less) \
| less \
| cut -f1 \
| sort -u \
| less) ;
echo "$uniqueepitopesvargas" \
| cat -n ;
## 529 unique IEDB epitopes in Vargas 2021 paper! So, let's add them!
awk -F'\t' 'NR==FNR {menardopeps[$1] ; next} !($1 in menardopeps)' OFS='\t' <(less iedbepitopes_all_menardo2021.tsv | cut -f7) <(less iedbepitopes_vargas2021_original.csv | sed -e 's/,/\t/g' -e '1d' | cut -f1 | sort -u | less) \
| less \
| wc -l ;
## same number of unique Vargas2021 epitopes as above, confirmed!

## Vargas 2021 only has start_peptide_genomic_coord	and end_peptide_genomic_coord!
head -2 iedbepitopes_vargas2021_original.csv ;
grep -e 'AADMWGPSSDPAWER' -e 'Nov' -m 3 iedbepitopes_vargas2021_original.csv ;
## it is not possible to trust in the coordinate data provided by this paper. In the peptide case, peptide coordinates are accurate but the gene coordinates are not. Confirmed with https://www.ncbi.nlm.nih.gov/protein/NP_216402.1?report=graph and https://www.ncbi.nlm.nih.gov/gene/885785
### solution is to grep each unique epitope to H37Rv protein fasta (Menardo coordinates will be assumed for others) in order to get peptide coordinates and use that to calculate genomic coordinates!


echo "Download and inspect H37Rv RefSeq protein fasta" ;
#---------------------------------------------------------------------------------------
wget "https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?tool=portal&save=file&log$=seqview&db=nuccore&report=fasta_cds_aa&id=448814763&" -O  - | less \
| fasta_formatter -w 0 >H37Rv_protein.fasta ;
head H37Rv_protein.fasta ;
## protein sequence in 1-letter code
## protein fasta headers contain gene_name, locus_tag, start-end coordinates with indication of "-" strand genes and other infos...
wc -l H37Rv_protein.fasta ;
## 7812/2=3906 CDSs!

grep '>' H37Rv_protein.fasta \
| grep 'complement' -C2 -m 3 ;
grep '>' H37Rv_protein.fasta \
| less \
| sed -e 's/.*\[location=/\+\t/g' -e 's/\].*//g' -e 's/.*(/-\t/g' -e 's/\.\./\t/g' -e 's/)//g' \
| less \
| head ;
## this is a way to get strand and start-end coordinates of each gene!
echo "ab" | grep 'b' -o -b ;
## coordinate of "b" was 1, so it's 0-based, it is needed to add +1


echo "Get protein and genomic coordinates of each unique Vargas2021 epitope" ;
#---------------------------------------------------------------------------------------
echo "$uniqueepitopesvargas" \
| less \
| while read epitope ;
  do epitopematch=$(grep "$epitope" H37Rv_protein.fasta -B1) ;
    peptidecoords=$(echo "$epitopematch" | tail -1 | grep -o -b "$epitope" | awk -v epitope="$epitope" -F':' '{print length(epitope),$1+1,$1+length(epitope)}' OFS='\t') ;
    epitopematch_length=$(echo "$epitopematch" | wc -l) ;
    if [ -z "$epitopematch" ] ; then echo -e "$epitope""\t"notfound ; 
    elif [ "$epitopematch_length" -gt 2 ] ; 
      then echo -e "$epitope""\t"multiplematches ;
      else echo "$epitopematch" \
        | head -1 \
        | sed -e 's/.*\[location=/\+\t/g' -e 's/\].*//g' -e 's/.*(/-\t/g' -e 's/\.\./\t/g' -e 's/)//g' \
        | less \
        | awk -v epitope="$epitope" -v peptidecoords="$peptidecoords" '{print epitope,peptidecoords,$0}' OFS='\t' \
        | less ;
    fi ; 
done \
| less \
| awk -F'\t' '{if($5!="") print $0,$3*3-2,$4*3 ; else print $0}' OFS='\t' \
| less \
| awk -F'\t' '{if($5=="+") print $0,$6+$(NF-1)-1,$6+$NF-1 ; else if($5=="-") print $0,$7-$NF+1,$7-$(NF-1)+1 ; else print $0}' OFS='\t' \
| less \
>iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
head -2 iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
wc -l iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
## 529 rows, confirmed!

## manual confirmation of peptide and gene coordinates:
echo "MAEPLAVDPTGLSAAAAKLAGLVFPQPPAPIAV" \
| awk '{print length($1)}'
## length=33. wc -c reports 34 (must be counting the end of line char)
## https://mycobrowser.epfl.ch/genes/Rv3878
tail iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
## coordinates confirmed with '+' gene https://www.ncbi.nlm.nih.gov/gene/886830

# get peptide and gene coordinates for notfound epitopes:
grep 'notfound' iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv \
| cat -n ;
## 35 epitopes were not found in H37Rv. Those might be coming from pseudogenes or genes deleted in latest H37Rv annotations... So, let's check them in Mycobrowser:
wget "https://mycobrowser.epfl.ch/releases/4/get_file?dir=fasta_proteins&file=Mycobacterium_tuberculosis_H37Rv.fasta" -O  - \
| fasta_formatter -w 0 >H37Rv_protein_mycobrowser.fasta ;

grep 'notfound' iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv \
| less \
| cut -f1 \
| less \
| grep -F -f /dev/stdin -B1 H37Rv_protein_mycobrowser.fasta ;
## only a single epitope found, which will be added manually to the previous file:
### pep_coords: 12-26 ; gene_coords: 223607+3*12-3=223640 ; 223607+3*26-3=223682 (https://mycobrowser.epfl.ch/genes/Rv0192A)
echo -e SLFAALNIAAVVAVL"\t"15"\t"12"\t"26"\t"+"\t"a"\t"a"\t"a"\t"a"\t"223640"\t"223682 >>iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
tail -2 iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv ;
## addition was successful!
grep -w 'SLFAALNIAAVVAVL' iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv
### however, this peptide was already there as 'notfound' and so is now duplicated!...


echo "Create MHC2 peptide epitope list" ;
#---------------------------------------------------------------------------------------
## vargas2021 will be discarded, as it does not have MHC1or2 info...
less iedbepitopes_all_menardo2021.tsv \
| awk -F'\t' '$4 !~ "MHC1" {print $7}' OFS='\t' \
| sort -u \
| less \
>iedbepitopes_mhc2only_menardo2021.mhc2 ;
wc -l iedbepitopes_mhc2only_menardo2021.mhc2 ;
## 1144 unique MHC2-binding peptides!
head iedbepitopes_mhc2only_menardo2021.mhc2 ;
less iedbepitopes_mhc2only_menardo2021.mhc2 \
| sort \
| uniq -c \
| awk '$1>1' \
| wc -l ;
## no duplicated peptides present, good!


echo "Create file for annotation" ;
#---------------------------------------------------------------------------------------
less iedbepitopes_all_menardo2021.tsv \
| datamash -s -g 1,2,3 unique 4 \
| less \
| cat /dev/stdin <(less iedbepitopes_vargas2021_uniqueepitopes_genecoords.tsv | grep -v -e 'notfound' -e 'multiplematches' | less | awk -F'\t' '{print "MTB_anc",$(NF-1),$NF,"!IEDBvargas2021"}' OFS='\t' | less) \
| sort -nk2,3 \
| sed '1i#CHROM\tFROM\tTO\tIEDB_tag' \
| less \
>iedbepitopes_forannotation.tab ;
head -2 iedbepitopes_forannotation.tab ;
wc -l iedbepitopes_forannotation.tab ;
## 1939 genomic intervals!, some are overlapping...


echo "Create input for bcftools annotate: bgzip + tabix index + header" ;
#http://www.htslib.org/doc/bcftools.html#annotate ; https://github.com/samtools/bcftools/wiki/HOWTOs#annotate-from-bed ;
cp iedbepitopes_forannotation.tab iedbepitopes_annot.tab ;
bgzip --force iedbepitopes_annot.tab ;
tabix --sequence 1 --begin 2 --end 3 --force iedbepitopes_annot.tab.gz ;


echo "Create VCF headers for IEDB epitopes" ;
##http://www.htslib.org/doc/bcftools.html#annotate
echo '##INFO=<ID=IEDB_tag,Number=.,Type=String,Description="IEDB epitope MHC type and tag">' \
| tee iedbepitopes_annot.header ;
ls -ltrh | tail ;


echo "Test annotation process of IEDB epitopes" ;
##http://www.htslib.org/doc/bcftools.html#annotate
##tag must be the same in header and indexed tab file!
time zcat ERR2653038_bcftools_varsonly.vcf.gz \
| bcftools annotate \
--annotations iedbepitopes_annot.tab.gz \
--header-lines iedbepitopes_annot.header \
--columns CHROM,POS,REF,ALT,IEDB_tag \
--merge-logic IEDB_tag:unique \
--threads 12 --output-type u \
| bcftools query -f'[%CHROM\t%POS\t%REF\t%ALT\t%IEDB_tag\n]' \
| grep '!IEDB' ;
# 0.03s!
## There seems to be no epitope variant in this sample! We might have to check other samples...
### still, no error, so the annotation process worked!

ls -ltrh ;

