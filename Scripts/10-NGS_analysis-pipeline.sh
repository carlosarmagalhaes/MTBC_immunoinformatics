
cd ../NGS_analysis ;
conda deactivate ; conda activate samtools12 ;


echo "Get list of genomes to process" ;
#---------------------------------------------------------------------------------------
cat -n notprocessed_genomes.txt ;
## 32 genomes were found to be either single-end wrongly tagged as 'PAIRED' (e.g. https://www.ncbi.nlm.nih.gov/sra/?term=ERR234214) or did not pass SRA extraction!
less ../Dataset_definition/downloadlist_final.tsv \
| cut -f1 \
| grep -v -w -F -f <(cat notdownloaded_genomes.txt notprocessed_genomes.txt | less) /dev/stdin \
| less \
>listofgenomes_forpipeline.txt ;
cat -n listofgenomes_forpipeline.txt ;
## 13064 genomes to submit to NGS pipeline!


echo "Create equally-sized intervals file for the reference genome" ;
#---------------------------------------------------------------------------------------
threads=24 ;
export threads ;
##inspired on https://github.com/jodyphelan/TBProfiler/blob/master/pathogenprofiler/bam.py
bedtools makewindows -g ../NGS_helper_files/MTB_anc.fasta.fai -n "$threads" -i winnum \
| awk '{print $1":"$2+1"-"$3,"part"$4}' \
>intervals_"$threads"threads.bed ;
cat -n intervals_"$threads"threads.bed ;

#------------------------------------------------------


total_genomes=$(cat listofgenomes_forpipeline.txt | wc -l) ;
echo total_genomes="$total_genomes" ;

i=24 ;
export i ;
## process "$i" genomes at a time:
less listofgenomes_forpipeline.txt \
| less \
| awk -v i="$i" 'ORS=NR%i?FS:RS' \
| less \
| sed -e 's/ $//g' \
| awk '{print $0}' \
| sed -e 's/ /!/g' \
| less \
| while read files ;
  do echo "$i" / "$total_genomes" ;
    
    
    echo "Process each $i genome individually until mapping (inclusive)" ;
    #---------------------------------------------------------------------------------------
    echo "$files" \
    | tr "!" "\n" \
    | awk '{print $0}' \
    | while read sample ;
      do echo sample="$sample" ;
      
        
        echo "1. Extract fastq files" ;
        #---------------------------------------------------------------------------------------
        fasterq-dump --skip-technical --split-3 --threads 6 --progress "$sample".sra --outfile "$sample" ;
        
        # remove SRA file and unmated reads (if present)
        ##https://github.com/ncbi/sra-tools/wiki/HowTo:-fasterq-dump#1-fastqfasta--split-3
        rm "$sample".sra "$sample".fastq ;
        
        
        echo "2. Read trimming with BBDuk" ;
        #---------------------------------------------------------------------------------------
        bbduk.sh \
        in1="$sample"_1.fastq in2="$sample"_2.fastq \
        out="$sample"_R1_bbduk.fastq out2="$sample"_R2_bbduk.fastq \
        overwrite=t \
        ref=../NGS_helper_files/adapters_combined_256_unique.fasta \
        ftm=5 \
        ktrim=r k=19 mink=8 editdistance=1 editdistance2=1 \
        trimpairsevenly=f removeifeitherbad=t \
        qtrim=r trimq=20 \
        trimpolygright=10 \
        minavgquality=20 \
        minlength=20 ottm=t \
        rename=t ziplevel=1 showspeed=t ;
        ## remove original fastq files:
        rm *_[1-2].fastq ; 

    
        echo "3. Get Kraken2 report" ;
        #---------------------------------------------------------------------------------------
        conda deactivate ; conda activate kraken2_newest ;
        KRAKEN2_DB_PATH="/home/ramdisk/" ;
        export KRAKEN2_DB_PATH ;
    
        kraken2 --memory-mapping --db kraken2_standard --paired --minimum-base-quality 20 --confidence 0 --use-names --threads "$i" --output "-" --report "$sample"_ramkraken2report.tsv "$sample"_R1_bbduk.fastq "$sample"_R2_bbduk.fastq ; 
        
        #Kraken2 report:
        conda deactivate ; conda activate samtools12 ;
        less "$sample"_ramkraken2report.tsv \
        | awk -F'\t' '$NF=="unclassified" || $NF ~ "Mycobacterium tuberculosis complex" {print $1}' OFS='\t' \
        | tr "\n" "\t" \
        | sed -e 's/ //g' \
        | awk -v sample="$sample" -F'\t' '{print sample,$2,$1,100-($1+$2)}' OFS='\t' \
        | less \
        >"$sample"_mtbcreadcheck.tsv ;
        
        echo "Another $i genomes extracted, trimmed and tested for contamination!" ;
  
      
        echo "4. Map genomes to MTB_anc with bwa mem + sort bam by read name" ;
        #---------------------------------------------------------------------------------------
        bwa mem -t "$i" MTB_anc.fasta "$sample"_R1_bbduk.fastq "$sample"_R2_bbduk.fastq \
        | samtools sort -n -l 1 -@ "$i" -o "$sample".bam ;
        rm "$sample"_R1_bbduk.fastq "$sample"_R2_bbduk.fastq ;
        echo "$i" / "$total_genomes" ;
      
    done ;
    echo "Another $i genomes trimmed with BBDuk and mapped with bwa mem" ;
    ls -ltrh | tail ;

    
    echo "5. Mark duplicates with samtools markdup in parallel ($i / 2) threads x 2 per file + samtools index" ;
    #---------------------------------------------------------------------------------------
    halfthreads=$(echo "$threads" | awk -v threads="$threads" '{print threads / 2}') ;
    ls *.bam \
    | less \
    | sed -e 's/\.bam//g' \
    | less \
    | parallel -j"$halfthreads" "samtools fixmate -m -@ 2 {1}.bam -u - | samtools sort -u -@ 2 | samtools markdup --include-fails -S --mode s -@ 2 - -O bam,level=1 {1}_markdup.bam ; samtools index -@ 2 {1}_markdup.bam ; rm {1}.bam" ;
    
    ls -ltrh | tail ;
    echo "Duplicate marking and indexing in another $i genomes finished" ;
    
    
    echo "6. Calculate bam coverage with mosdepth in parallel (with mate pair overlap correction)" ;
    #---------------------------------------------------------------------------------------
    mosdepth_bin=$(echo "$CONDA_PREFIX"/bin/mosdepth | sed -e 's/samtools12/mosdepth/g') ;
    echo mosdepth_bin="$mosdepth_bin" ;
    export mosdepth_bin ;
    
    # spin up parallel jobs:
    ##https://github.com/brentp/mosdepth
    time ls *.bam \
    | less \
    | awk -F'_' '{print $0,$1}' OFS='\t' \
    | less \
    | parallel -j0 --colsep="\t" '"$mosdepth_bin" --flag 3844 --mapq 20 --use-median --threads 1 {2} {1}' ;
    ## 3s only!!
    echo "Mapped coverage calculated for another $i genomes" ;

    
    echo "Process each $i genome to get coverage metrics and to perform variant calling and annotation" ;
    #---------------------------------------------------------------------------------------
    for bam in *.bam ; 
      do sample=$(echo "$bam" | sed -e 's/_.*//g') ;
        echo sample="$sample" ;
        export sample ;
        
        
        echo "7. Extract coverage metrics" ;
        #---------------------------------------------------------------------------------------
        awk 'NR==2 {print $4}' OFS='\t' "$sample"*mosdepth.summary.txt \
        | less \
        | cat /dev/stdin <(awk '$2==10 {print $3}' OFS='\t' "$sample"*mosdepth.global.dist.txt | head -1 | less) \
        | less \
        | tr "\n" "\t" \
        | awk -v sample="$sample" -F'\t' '{print sample,$0}' OFS='\t' \
        | less \
        >"$sample"_coveragecheck.tsv ;
        ## $2: mean coverage ; $3: coverage breadth at >=10 reads

        
        echo "7.1. Get genomic positions without mapped coverage" ;
        #---------------------------------------------------------------------------------------
        less "$sample"*per-base* \
        | awk -F'\t' '$NF==0' OFS='\t' \
        | less \
        >"$sample"_zerocoverageregions.tsv ;
        
        # remove mosdepth outputs:
        rm "$sample"*mosdepth* "$sample"*per-base* ;


        echo "8. Variant calling with bcftools parallel jobs + bgziptabix index + bcftools concat + bcftools filter + bcftools view + bcftools csq" ;
        #---------------------------------------------------------------------------------------
        cat intervals_"$threads"threads.bed \
        | parallel -j "$threads" --col-sep " " 'bcftools mpileup "$sample"_markdup.bam \
        --fasta-ref ../NGS_helper_files/MTB_anc.fasta \
        --count-orphans \
        --no-BAQ \
        --max-depth 100000 \
        --min-MQ 20 \
        --min-BQ 20 \
        --regions {1} \
        --annotate AD,ADF,ADR,DP,SP,QS,SCR,INFO/AD,INFO/ADF,INFO/ADR,INFO/SCR \
        --threads 1 \
        --output-type u \
        | bcftools call --ploidy 1 \
        --keep-alts --keep-masked-ref \
        --multiallelic-caller \
        --annotate INFO/PV4 \
        --variants-only \
        --threads 1 \
        --output-type u \
        | bcftools +fill-tags --threads 1 --output-type u -- --tags FORMAT/VAF1 \
        | bcftools norm --fasta-ref ../NGS_helper_files/MTB_anc.fasta \
        --multiallelics - \
        --old-rec-tag SPLITVAR \
        --keep-sum AD \
        --threads 1 \
        --output-type v \
        | bcftools +fill-tags --threads 1 --output-type u -- --tags FORMAT/VAF \
        | bcftools annotate \
        --annotations ../NGS_helper_files/excludedloci_RLC2021_annot.tab.gz \
        --header-lines ../NGS_helper_files/excludedloci_RLC2021_annot.header \
        --columns CHROM,FROM,TO,RLC_tag \
        --threads "$threads" --output-type u \
        | bcftools annotate \
        --annotations ../NGS_helper_files/blindspots_mappability_marin2021_annot.tab.gz \
        --header-lines ../NGS_helper_files/blindspots_mappability_marin2021_annot.header \
        --columns CHROM,FROM,TO,Mappability \
        --threads "$threads" --output-type u \
        | bcftools annotate \
        --annotations ../NGS_helper_files/lineagesnps_annot.tab.gz \
        --header-lines ../NGS_helper_files/lineagesnps_annot.header \
        --columns CHROM,POS,REF,ALT,Lineage_tag \
        --threads "$threads" --output-type u \
        | bcftools annotate \
        --annotations ../NGS_helper_files/iedbepitopes_annot.tab.gz \
        --header-lines ../NGS_helper_files/iedbepitopes_annot.header \
        --columns CHROM,POS,REF,ALT,IEDB_tag \
        --merge-logic IEDB_tag:unique \
        --threads "$threads" --output-type v \
        | bgziptabix "$sample"_bcftools_varsonly_{2}.vcf.gz' ;
        ls -ltrh | tail -"$threads" ; 
  
    
        echo "8.1. Concatenate partial VCFs + index final one (bgzip + tabix)" ;
        #---------------------------------------------------------------------------------------
        ##https://github.com/jodyphelan/TBProfiler/blob/master/pathogenprofiler/bam.py
        ##https://www.biostars.org/p/59492/#59509
        bcftools concat --allow-overlaps --remove-duplicates --threads "$threads" --output-type v *part*.vcf.gz \
        | bgziptabix "$sample"_bcftools_varsonly_annotated.vcf.gz ;
        ##'--allow-overlaps --remove-duplicates' required prior indexing, while use of "--naive" option required vcf files to be passed in the right order (no sorting) and it would not have been possible to remove duplicates...
        
        # remove parcial VCFs and respective indexes:
        rm *part*.vcf.gz *part*.vcf.gz.tbi ;
        
        
        echo "8.2. Annotate concatenated VCF with filter expressions and others" ;
        #---------------------------------------------------------------------------------------
        ## performed separately to avoid issues with single quotes inside GNU parallel...
        bcftools filter --mode + \
        -s LowQual20 --exclude 'QUAL<20' \
        --output-type u "$sample"_bcftools_varsonly_annotated.vcf.gz \
        | bcftools filter --mode + \
        -s LowMQ40 --exclude 'MQ<40' \
        --output-type u \
        | bcftools filter --mode + \
        -s LowVAF1_75 --exclude 'FORMAT/VAF1<0.75' \
        --output-type u \
        | bcftools filter --mode + \
        -s LowVAF75 --exclude 'FORMAT/VAF<0.75' \
        --output-type u \
        | bcftools filter --mode + \
        -s HalfMix --exclude 'FORMAT/VAF=0.5' \
        --output-type u \
        | bcftools filter --mode + \
        -s LowVAF50 --exclude 'TYPE="indel" && FORMAT/VAF<0.5' \
        --output-type u \
        | bcftools filter --mode + \
        -s RLC --exclude 'RLC_tag!=""' \
        --output-type u \
        | bcftools annotate \
        --annotations ../NGS_helper_files/blindspots_EBRscore_marin2021_annot.tab.gz \
        --header-lines ../NGS_helper_files/blindspots_EBRscore_marin2021_annot.header \
        --columns CHROM,FROM,TO,EBR \
        --threads "$threads" --output-type u \
        | bcftools filter --mode + \
        -s LowEBR90 --exclude 'EBR<0.9' \
        --threads "$threads" --output-type u \
        | bcftools filter --mode + \
        -s LowMappability1 --exclude 'Mappability!="1"' \
        --output-type u \
        | bcftools filter --mode + \
        -s SetREF --exclude 'GT="alt" && FORMAT/VAF1<0.1' \
        --set-GTs 0 \
        --output-type v \
        | bcftools reheader --samples <(echo "$sample") \
        | grep -v 'Command=' \
        | bgziptabix "$sample"_bcftools_varsonly_annotated_withfilters.vcf.gz ;
        
        # remove previous VCF file and its index:
        rm *_annotated.vcf.gz *_annotated.vcf.gz.tbi ;
        ls -ltrh | tail -3 ;
        
        
        echo "8.3. Select only candidate variants from VCF to apply Gap distance filters" ;
        #---------------------------------------------------------------------------------------
        bcftools view -i '(TYPE="indel" && FORMAT/VAF>=0.5) || FORMAT/VAF>=0.75' --output-type u "$sample"_bcftools_varsonly_annotated_withfilters.vcf.gz \
        | bcftools filter --mode + --SnpGap 10 --IndelGap 3 \
        -s SetMissing --exclude 'GT="mis"' --output-type u \
        | bcftools csq \
        --fasta-ref ../NGS_helper_files/MTB_anc.fasta \
        --gff-annot ../NGS_helper_files/ncbi_convertedtoensembl.gff \
        --phase s \
        --output-type v \
        | grep -v 'Command=' \
        | bgziptabix "$sample"_bcftools_varsonly_annotated_withfilters_VAFselected.vcf.gz ;

  done ;
  echo "Variant calling and annotation concluded for another $i genomes" ;


  #remove bam files and respective indexes:
  rm *.bam *.bai ;

  i=$(echo "$i" | awk -v threads="$threads" '{print $1+threads}') ;
  export i ;
  echo "$i" / "$total_genomes" ;

done ;

# rm *.sra ;
ls -ltrh | head ;
ls -ltrh | tail ;
