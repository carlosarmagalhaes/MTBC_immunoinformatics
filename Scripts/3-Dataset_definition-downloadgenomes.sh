mkdir -p ../NGS_analysis ;
cd ../NGS_analysis ;

echo "Download list of genomes" ;
#---------------------------------------------------------------------------------------
less ../Dataset_definition/downloadlist_final.tsv \
| parallel --colsep='\t' -j3 --retries 3 "wget --quiet {2} -O {1}.sra" ;


echo "Redownload zero byte files" ;
#---------------------------------------------------------------------------------------
## 0-byte files were created in cases where AWS links were not found! 
## https://stackoverflow.com/questions/15703664/find-all-zero-byte-files-in-directory-and-subdirectories
find . -size 0 \
| sed -e 's/\.sra//g' \
| awk -F'/' '{print $NF}' \
| less \
>download_zerobytefiles.tsv ;
wc -l download_zerobytefiles.tsv ;
## 373 genomes to redownload!

# delete zero-byte files:
#------------------------------
find $HOME/ironwolf/representativelist/ -size 0 \
| less \
| xargs rm ;
## 0-byte files deleted!!


echo "Redownload files from NCBI" ;
#---------------------------------------------------------------------------------------
### it was concluded after some testing that it was too slow to download from http NCBI links! So, let's try to download the missing genomes with prefetch!
## sra-tools 2.11.0
time less download_zerobytefiles.tsv \
| parallel -j5 --delay 10 --ungroup --progress --verbose --retries 3 prefetch --output-directory $PWD/ {} ;
### if already downloaded, program skips files automatically ;)
### in case of fail, program does not create a folder...
ls -d */ \
| sed -e 's/\///g' \
| grep -v -w -F -f /dev/stdin download_zerobytefiles.tsv \
| less \
>notdownloaded_genomes.txt
cat -n notdownloaded_genomes.txt ;
## 4 genomes not downloaded: ERR845308, ERR760780, ERR718491 and ERR751357! -> SRA files absent in both NCBI and ENA records! (e.g. https://trace.ncbi.nlm.nih.gov/Traces/sra/?run=ERR845308 or https://trace.ncbi.nlm.nih.gov/Traces/sra/?run=ERR845308)

## prefetch created subfolders for each genome, so we need to move the contents and remove the subfolders!
ls -d */* \
| xargs mv -v -f -t $PWD/ ;
ls -d */* \
| xargs rm -rf ;
