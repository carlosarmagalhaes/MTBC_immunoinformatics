
cd ../NGS_helper_files ;


echo "Get Mykrobe lineage SNPs" ;
#---------------------------------------------------------------------------------------
mykrobe panels describe \
| less \
| awk 'NR==8 {print $4}' \
| less \
| xargs wget -O mykrobe.panel.tb.20201014.tar.gz ;

tar -xzvf mykrobe.panel.tb.20201014.tar.gz ;
head mykrobe.panel.tb.20201014/tb.lineage.20200930.json ;
cp mykrobe.panel.tb.20201014/tb.lineage.20200930.json $PWD/mykrobe_panel_tb_20201014_tb_lineage_20200930.json ;
rm -rf mykrobe.panel.tb.20201014.tar.gz mykrobe.panel.tb.20201014/ ;
ls -ltrh ;

cat mykrobe_panel_tb_20201014_tb_lineage_20200930.json \
| less \
| sed -e '/^  }/d' -e '/ref_allele/d' -e '/^{/d' -e '/^}/d' -e '/2-8/d' -e 's/report_name/name/g' -e 's/.*name": //g' -e 's/,$//g' -e 's/: {//g' -e 's/ //g' -e 's/"//g' \
| less \
| awk -F'\t' 'ORS=NR%2?FS:RS' \
| less \
| awk -F'\t' '{print substr($1,2,length($1)-2),substr($1,1,1),substr($1,length($1),9),"mykrobe",$2,$1}' OFS='\t' \
| less \
| sed -e 's/lineage//g' -e 's/clade_animal_/Animal/g' -e 's/Bovis/M.bovis/g' -e 's/Caprae/M.caprae/g' \
| less \
>lineagesnps_mykrobe.tsv ;
cat -n lineagesnps_mykrobe.tsv ;
## 70 SNPs!


echo "Get fast-lineage-caller SNPs" ;
#---------------------------------------------------------------------------------------
wget "https://github.com/farhat-lab/fast-lineage-caller/raw/master/snp_schemes/freschi_hierarchical.tsv" -O lineagesnps_freschi_original.tsv ;

less lineagesnps_freschi_original.tsv \
| awk -F'\t' 'NR>1 {print $2,substr($3,1,1),substr($3,3,9),$NF,$1}' OFS='\t' \
| less \
| sed -e 's/\*//g' -e 's/_hierarchical//g' \
| less \
>lineagesnps_freschi.tsv ;
cat -n lineagesnps_freschi.tsv ;
## 95 SNPs!


echo "Get TBprofiler lineage SNPs" ;
#---------------------------------------------------------------------------------------
wget "https://github.com/jodyphelan/TBProfiler/raw/cfb370ac2ca6d9ec0b7e3b39d0a9c929ec648639/db/tbdb.barcode.bed" -O lineagesnps_tbprofiler_original.bed ;

cat lineagesnps_tbprofiler_original.bed \
| less \
| awk -F'\t' '{print $3-$2}' \
| sort -u ;
## all 1048 intervals with 1bp distance and no indels in the list!

## Get REF nucleotide from MTBC ancestral:
#------------------------------------------------------
wget "https://github.com/jodyphelan/TBProfiler/raw/master/db/tbdb.fasta" -O - \
| less \
| sed '1d' \
| tr "\n" "," \
| awk '{print $0}' \
| less \
| sed -e '1i>H37Rv_tbdb' -e 's/,//g' \
| less \
>h37rv_tbdb.fasta ;
gzip h37rv_tbdb.fasta ;
ls -ltrh h37rv_tbdb.fasta.gz ;
## filesize reduced from 4.3MB to 1.2MB!

awk 'NR==2' MTB_anc.fasta \
| wc -L ;
zcat h37rv_tbdb.fasta.gz \
| tail -1 \
| wc -L ;
## length of H37Rv genome confirmed = 4411532 bp!

cat lineagesnps_tbprofiler_original.bed \
| less \
| while read line ; 
  do startpos=$(echo "$line" | awk -F'\t' '{print $2}') ;
    endpos=$(echo "$line" | awk -F'\t' '{print $3}') ;
    
    h37rv_startposbase=$(zcat h37rv_tbdb.fasta.gz | tail -1 | cut -c "$startpos")
    h37rv_endposbase=$(zcat h37rv_tbdb.fasta.gz | tail -1 | cut -c "$endpos")
    mtbanc_base=$(awk 'NR==2' MTB_anc.fasta | cut -c "$endpos") ;
    echo -e "$line""\t""$mtbanc_base""\t""$h37rv_startposbase""\t""$h37rv_endposbase" ;
    
  done \
| less \
| awk -F'\t' '{print $3,$(NF-2),$5,"tbprofiler",$4,$6,$7,$8,$5,$(NF-1),$NF}' OFS='\t' \
| less \
| sed -e 's/Lineage//g' -e 's/lineage//g' -e 's/ //g' \
| less \
>lineagesnps_tbprofiler.tsv ;

head -2 lineagesnps_tbprofiler.tsv ;
awk -F'\t' '$3==$NF' OFS='\t' lineagesnps_tbprofiler.tsv \
| less \
| cat -n ;
## 20 cases where lineage nucleotide == $h37rv_endposbase, but only in L4 or L4.9!
awk -F'\t' '$3==$(NF-1)' OFS='\t' lineagesnps_tbprofiler.tsv \
| less \
| wc -l ;
## 238 intervals where lineage (and not only in L4) nucleotide == $h37rv_endposbase -> so, it is concluded that the last interval of the original bed file (3rd column) is the correct genomic position to query MTBC ancestral genome, as confirmed in the next command!!
awk -F'\t' '$2==$3' OFS='\t' lineagesnps_tbprofiler.tsv ;
## no ocurrences where MTBC_anc base == lineage nucleotide marker!!


echo "Get Napier2020 lineage SNPs" ;
#---------------------------------------------------------------------------------------
## manually copied from the Table S3 of the paper (https://doi.org/10.1186/s13073-020-00817-3) and saved as 'lineagesnps_napier2020_90set_original.tsv'
less lineagesnps_napier2020_90set_original.tsv \
| awk -F'\t' '{print $2,substr($3,1,1),substr($3,4,9),"napier2020",$1,$0}' OFS='\t' \
| less \
>lineagesnps_napier2020_90set.tsv ;
cat -n lineagesnps_napier2020_90set_original.tsv ;

## Is this set contained in TBprofiler full set?
less lineagesnps_napier2020_90set.tsv \
| awk -F'\t' 'NR==FNR {posrefalt[$1 FS $2 FS $3] ; next} !($1 FS $2 FS $3 in posrefalt)' OFS='\t' lineagesnps_tbprofiler.tsv /dev/stdin \
| cat -n ;
### 2 SNPs, 1 from 4.9 and other from 4 were not found in TBprofiler full set! The rest of the SNPs were found...
### still, napier2020 set will be considered due to distinct sublineage scheme...

less lineagesnps_napier2020_90set.tsv \
| while read line ; 
  do interval=$(echo "$line" | awk -F'\t' '{print $1}') ;
    mtbanc_base=$(awk 'NR==2' MTB_anc.fasta | cut -c "$interval") ;
    echo -e "$line""\t""$mtbanc_base" ;
  done \
| less \
| awk -F'\t' '$2!=$NF' OFS='\t' \
| less \
| cut -f1 \
| grep -w -F -f /dev/stdin lineagesnps_tbprofiler.tsv ;
### those 2 mismatched SNPs are exactly the ones that have a distinct REF nucleotide in MTB_anc. Fortunately, TBprofiler fullset got the REF nucleotides right on these SNPs! Thus, SNPs will be corrected on napier2020 file (by swapping REF and ALT nucleotides):
awk -i inplace -F'\t' '{if($1==420008 || $1==2825466){refnuc=$2 ; altnuc=$3 ; $2=altnuc ; $3=refnuc ; print $0} else print $0}' OFS='\t' lineagesnps_napier2020_90set.tsv ;
grep -e '420008' -e '2825466' lineagesnps_napier2020_90set.tsv ;
## manual modification confirmed!


echo "Get Coscolla2021 L5 and L6 SNPs" ;
#---------------------------------------------------------------------------------------
## manually extracted from the Table S7 of the paper (https://doi.org/10.1099/mgen.0.000477) and saved as 'lineagesnps_coscolla2021_original.tsv'

grep ',' lineagesnps_coscolla2021_original.tsv ;
## some ALT nucleotides to spread across lines...
less lineagesnps_coscolla2021_original.tsv \
| sed '1d' \
| cut -f1 \
| sort -u \
| cat -n ;
## 16 lineage/sublineage tags to be replaced to the common representation (e.g. 5.1.1)
less lineagesnps_coscolla2021_original.tsv \
| awk -F'\t' 'NR>1 {lineagetag="" ; for(i=2;i<=length($1);i++){lineagetag=lineagetag"!"substr($1,i,1)"."} ; split($5,altnuc,",") ; for(i=1;i<=length($5);i++) print $2,$4,altnuc[i],"coscolla2021",lineagetag,$1,$0}' OFS='\t' \
| less \
| sed -e 's/!//g' -e 's/\.\t/\t/g' \
| less \
>lineagesnps_coscolla2021.tsv ;
head -2 lineagesnps_coscolla2021.tsv ;
wc -l lineagesnps_coscolla2021.tsv ;
## 664 SNPs!


echo "Join lineage SNPs from all sources for annotation" ;
#---------------------------------------------------------------------------------------
cat lineagesnps_mykrobe.tsv lineagesnps_freschi.tsv lineagesnps_tbprofiler.tsv lineagesnps_napier2020_90set.tsv lineagesnps_coscolla2021.tsv \
| less \
| datamash -s -g 1,2,3 unique 4,5 countunique 4,5 \
| less \
| while read line ; 
  do interval=$(echo "$line" | awk -F'\t' '{print $1}') ;
    mtbanc_base=$(awk 'NR==2' MTB_anc.fasta | cut -c "$interval") ;
    echo -e "$line""\t""$mtbanc_base" ;
  done \
| less \
| awk -F'\t' '{if($2!=$NF) print $0,"diff" ; else print $0}' OFS='\t' \
| less \
>lineagesnps_joined.tsv ;
head -2 lineagesnps_joined.tsv ;
wc -l lineagesnps_joined.tsv ;
## 1683 SNPs!
grep 'diff' lineagesnps_joined.tsv ;
## 3 H37RV-specific SNPs found to be discarded from VCF annotation!
less lineagesnps_joined.tsv \
| grep -v 'diff' \
| awk -F'\t' '$(NF-1)>1' OFS='\t' \
| cat -n ;
## 30 SNPs have >1 sublineage tags!

less lineagesnps_joined.tsv \
| grep -v 'diff' \
| datamash -s -g 1,2,3 unique 4,5 \
| less \
| awk -F'\t' '{print "MTB_anc",$1,$2,$3,"!lineage,"$5","$4}' OFS='\t' \
| sort -nk2,2 \
| sed '1i#CHROM\tPOS\tREF\tALT\tLineage_tag' \
| less \
>lineagesnps_forannotation.tab ;
head lineagesnps_forannotation.tab ;
wc -l lineagesnps_forannotation.tab ;
## 1680 SNPs!


echo "Create input for bcftools annotate: bgzip + tabix index + header" ;
#---------------------------------------------------------------------------------------
#http://www.htslib.org/doc/bcftools.html#annotate ; https://github.com/samtools/bcftools/wiki/HOWTOs#annotate-from-bed ;
cp lineagesnps_forannotation.tab lineagesnps_annot.tab ;
bgzip --force lineagesnps_annot.tab ;
tabix --sequence 1 --begin 2 --end 2 --force lineagesnps_annot.tab.gz ;

##http://www.htslib.org/doc/bcftools.html#annotate
echo '##INFO=<ID=Lineage_tag,Number=.,Type=String,Description="Lineage/sublineage tag">' \
| tee lineagesnps_annot.header ;

ls -ltrh | tail ;


echo "Test annotation process of lineage/sublinage SNPs" ;
#---------------------------------------------------------------------------------------
##http://www.htslib.org/doc/bcftools.html#annotate
##tag must be the same in header and indexed tab file!
zcat ERR2653038_bcftools_varsonly.vcf.gz \
| bcftools annotate \
--annotations lineagesnps_annot.tab.gz \
--header-lines lineagesnps_annot.header \
--columns CHROM,POS,REF,ALT,Lineage_tag \
--threads 12 --output-type u \
| bcftools query -f'[%CHROM\t%POS\t%REF\t%ALT\t%Lineage_tag\n]' \
| grep -e '!lineage' ;
## lineage/sublineage SNPs successfully annotated!

ls -ltrh ;

