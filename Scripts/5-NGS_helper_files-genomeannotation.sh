
cd ../NGS_helper_files ;


echo "Retrieve Refseq annotation" ;
#---------------------------------------------------------------------------------------
#download gff3 file directly from the website:
##https://www.ncbi.nlm.nih.gov/nuccore/NC_000962
### Send to -> Complete Record -> File -> GFF3
wget "https://www.ncbi.nlm.nih.gov/sviewer/viewer.cgi?tool=portal&save=file&log$=seqview&db=nuccore&report=gff3&id=448814763&" -O ncbi_web.gff ;


echo "Convert annotation to Ensembl format for use with bcftools csq" ;
#---------------------------------------------------------------------------------------
sed -e 's/^NC_000962.3/MTB_anc/g' -e 's/gene-/gene:/g' -e 's/rna-/transcript:/g' -e 's/biotype=tRNA/biotype=protein_coding/g' -e 's/biotype=ncRNA/biotype=lincRNA/g' ncbi_web.gff \
| less \
| awk -F'\t' '{if($3=="CDS"){split($NF,info,";") ; print $1,$2,$3,$4,$5,$6,$7,$8,info[2]";"$NF} else print $0}' OFS='\t' \
| less \
| sed -e 's/\tParent=gene:/\tID=CDS:/g' -e 's/ID=cds-.*Parent=gene/Parent=transcript/g' \
| less \
| awk -F'\t' '{if($3=="gene" && $NF ~ /protein_coding/)  {split($NF,info,";") ; print $0"\n"$1,$2,"mRNA",$4,$5,$6,$7,$8,"ID=transcript:"info[1]";Parent="info[1]";gene_biotype=protein_coding"} else print $0}' OFS='\t' \
| less \
| awk -F'\t' '{if($3=="pseudogene") {split($NF,info,";") ; print $1,$2,"gene",$4,$5,$6,$7,$8,"ID=transcript:"info[1]";Parent="info[1]";biotype=pseudogene\n"$0} else print $0}' OFS='\t' \
| less \
| sed -e 's/:ID=gene:/:/g' -e 's/=ID=/=/g' \
| less \
| awk -F'\t' '{if($3=="rRNA") print $0";biotype=rRNA" ; else print $0}' OFS='\t' \
| awk -F'\t' '{if($3=="tRNA") print $0";biotype=protein_coding" ; else print $0}' OFS='\t' \
| awk -F'\t' '{if($3=="ncRNA") print $0";biotype=lincRNA" ; else print $0}' OFS='\t' \
| awk -F'\t' '{if($3=="transcript") {$3="misc_RNA" ; print $0";biotype=misc_RNA"} else print $0}' OFS='\t' \
| awk -F'\t' '$3!="region" && $3!="mobile_genetic_element" && $3!="repeat_region" && $3!="sequence_feature"' OFS='\t' \
| less \
| sed -e 's/^$/###/g' \
>ncbi_convertedtoensembl.gff ;


echo "Test annotation of NC00962.3 converted to Ensembl format" ;
#---------------------------------------------------------------------------------------
##http://www.htslib.org/doc/bcftools.html#csq
zcat ERR2653038_bcftools_varsonly.vcf.gz \
| bcftools csq \
--fasta-ref MTB_anc.fasta \
--gff-annot ncbi_convertedtoensembl.gff \
--phase s --threads 12 --verbose /dev/stdin \
| bcftools query -f'[%CHROM\t%POS\t%TBCSQ{*}\n]' \
| less \
| grep -e 'non_coding' -e 'intron' -e 'Rv2804c' ;
## all biotypes successfully annotated! And also overlapping CDS positions... 
## no errors reported!

ls -ltrh ;

